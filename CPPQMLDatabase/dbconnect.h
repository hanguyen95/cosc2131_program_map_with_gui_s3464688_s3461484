#ifndef DBCONNECT_H
#define DBCONNECT_H
#include <QtSql/QSqlDatabase>

class DBConnect
{
public:
    DBConnect();
    QSqlDatabase con() const;
    void setType(int i);
    int getType() const;
    void doConnect(QString driver, QString hostname, QString databasename, QString username, QString password);
private:
    QSqlDatabase m_db;
    int type = -1;
};

#endif // DBCONNECT_H
