/****************************************************************************
** Meta object code from reading C++ file 'services.h'
**
** Created: Tue 17. Dec 13:33:27 2013
**      by: The Qt Meta Object Compiler version 67 (Qt 5.0.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../services.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'services.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.0.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Services_t {
    QByteArrayData data[20];
    char stringdata[231];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Services_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Services_t qt_meta_stringdata_Services = {
    {
QT_MOC_LITERAL(0, 0, 8),
QT_MOC_LITERAL(1, 9, 10),
QT_MOC_LITERAL(2, 20, 0),
QT_MOC_LITERAL(3, 21, 8),
QT_MOC_LITERAL(4, 30, 7),
QT_MOC_LITERAL(5, 38, 8),
QT_MOC_LITERAL(6, 47, 18),
QT_MOC_LITERAL(7, 66, 11),
QT_MOC_LITERAL(8, 78, 6),
QT_MOC_LITERAL(9, 85, 29),
QT_MOC_LITERAL(10, 115, 17),
QT_MOC_LITERAL(11, 133, 17),
QT_MOC_LITERAL(12, 151, 2),
QT_MOC_LITERAL(13, 154, 4),
QT_MOC_LITERAL(14, 159, 9),
QT_MOC_LITERAL(15, 169, 7),
QT_MOC_LITERAL(16, 177, 6),
QT_MOC_LITERAL(17, 184, 17),
QT_MOC_LITERAL(18, 202, 17),
QT_MOC_LITERAL(19, 220, 9)
    },
    "Services\0changeName\0\0username\0doLogin\0"
    "password\0getTransactionData\0getUserRole\0"
    "userID\0getTeachingAssistantUsernames\0"
    "getCourseOutlines\0updateTransaction\0"
    "id\0desc\0starttime\0endtime\0status\0"
    "insertTransaction\0deleteTransaction\0"
    "getUserID\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Services[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x0a,

 // methods: name, argc, parameters, tag, flags
       4,    2,   72,    2, 0x02,
       6,    1,   77,    2, 0x02,
       7,    1,   80,    2, 0x02,
       9,    0,   83,    2, 0x02,
      10,    0,   84,    2, 0x02,
      11,    5,   85,    2, 0x02,
      17,    5,   96,    2, 0x02,
      17,    4,  107,    2, 0x22,
      18,    1,  116,    2, 0x02,
      19,    1,  119,    2, 0x02,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // methods: parameters
    QMetaType::Bool, QMetaType::QString, QMetaType::QString,    3,    5,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::QString, QMetaType::Int,    8,
    QMetaType::QStringList,
    QMetaType::QStringList,
    QMetaType::Bool, QMetaType::Int, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,   12,   13,   14,   15,   16,
    QMetaType::Bool, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,   13,   14,   15,   16,
    QMetaType::Bool, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,    3,   13,   14,   15,
    QMetaType::Bool, QMetaType::Int,   12,
    QMetaType::Int, QMetaType::QString,    3,

       0        // eod
};

void Services::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Services *_t = static_cast<Services *>(_o);
        switch (_id) {
        case 0: _t->changeName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: { bool _r = _t->doLogin((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: _t->getTransactionData((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: { QString _r = _t->getUserRole((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 4: { QStringList _r = _t->getTeachingAssistantUsernames();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = _r; }  break;
        case 5: { QStringList _r = _t->getCourseOutlines();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->updateTransaction((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->insertTransaction((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])),(*reinterpret_cast< QString(*)>(_a[5])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: { bool _r = _t->insertTransaction((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: { bool _r = _t->deleteTransaction((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: { int _r = _t->getUserID((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject Services::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Services.data,
      qt_meta_data_Services,  qt_static_metacall, 0, 0}
};


const QMetaObject *Services::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Services::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Services.stringdata))
        return static_cast<void*>(const_cast< Services*>(this));
    return QObject::qt_metacast(_clname);
}

int Services::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
