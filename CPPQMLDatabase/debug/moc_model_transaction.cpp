/****************************************************************************
** Meta object code from reading C++ file 'model_transaction.h'
**
** Created: Tue 17. Dec 13:33:27 2013
**      by: The Qt Meta Object Compiler version 67 (Qt 5.0.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../model_transaction.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'model_transaction.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.0.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Model_Transaction_t {
    QByteArrayData data[12];
    char stringdata[117];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Model_Transaction_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Model_Transaction_t qt_meta_stringdata_Model_Transaction = {
    {
QT_MOC_LITERAL(0, 0, 17),
QT_MOC_LITERAL(1, 18, 9),
QT_MOC_LITERAL(2, 28, 0),
QT_MOC_LITERAL(3, 29, 11),
QT_MOC_LITERAL(4, 41, 16),
QT_MOC_LITERAL(5, 58, 14),
QT_MOC_LITERAL(6, 73, 11),
QT_MOC_LITERAL(7, 85, 2),
QT_MOC_LITERAL(8, 88, 4),
QT_MOC_LITERAL(9, 93, 9),
QT_MOC_LITERAL(10, 103, 7),
QT_MOC_LITERAL(11, 111, 4)
    },
    "Model_Transaction\0idChanged\0\0descChanged\0"
    "starttimeChanged\0endtimeChanged\0"
    "statChanged\0id\0desc\0starttime\0endtime\0"
    "stat\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Model_Transaction[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       5,   44, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x05,
       3,    0,   40,    2, 0x05,
       4,    0,   41,    2, 0x05,
       5,    0,   42,    2, 0x05,
       6,    0,   43,    2, 0x05,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       7, QMetaType::Int, 0x00495001,
       8, QMetaType::QString, 0x00495001,
       9, QMetaType::QString, 0x00495001,
      10, QMetaType::QString, 0x00495001,
      11, QMetaType::QString, 0x00495001,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void Model_Transaction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Model_Transaction *_t = static_cast<Model_Transaction *>(_o);
        switch (_id) {
        case 0: _t->idChanged(); break;
        case 1: _t->descChanged(); break;
        case 2: _t->starttimeChanged(); break;
        case 3: _t->endtimeChanged(); break;
        case 4: _t->statChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Model_Transaction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Model_Transaction::idChanged)) {
                *result = 0;
            }
        }
        {
            typedef void (Model_Transaction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Model_Transaction::descChanged)) {
                *result = 1;
            }
        }
        {
            typedef void (Model_Transaction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Model_Transaction::starttimeChanged)) {
                *result = 2;
            }
        }
        {
            typedef void (Model_Transaction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Model_Transaction::endtimeChanged)) {
                *result = 3;
            }
        }
        {
            typedef void (Model_Transaction::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Model_Transaction::statChanged)) {
                *result = 4;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Model_Transaction::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Model_Transaction.data,
      qt_meta_data_Model_Transaction,  qt_static_metacall, 0, 0}
};


const QMetaObject *Model_Transaction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Model_Transaction::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Model_Transaction.stringdata))
        return static_cast<void*>(const_cast< Model_Transaction*>(this));
    return QObject::qt_metacast(_clname);
}

int Model_Transaction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = id(); break;
        case 1: *reinterpret_cast< QString*>(_v) = desc(); break;
        case 2: *reinterpret_cast< QString*>(_v) = starttime(); break;
        case 3: *reinterpret_cast< QString*>(_v) = endtime(); break;
        case 4: *reinterpret_cast< QString*>(_v) = stat(); break;
        }
        _id -= 5;
    } else if (_c == QMetaObject::WriteProperty) {
        _id -= 5;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Model_Transaction::idChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void Model_Transaction::descChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void Model_Transaction::starttimeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void Model_Transaction::endtimeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void Model_Transaction::statChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}
QT_END_MOC_NAMESPACE
