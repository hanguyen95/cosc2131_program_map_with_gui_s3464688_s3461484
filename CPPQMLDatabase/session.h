#ifndef SESSION_H
#define SESSION_H

#include <QObject>
#include "student.h"
#include "manager.h"

class Session : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString username READ getUsername WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY(QString current_sem READ getCurrentSem NOTIFY getCurrentSemChanged)
    Q_PROPERTY(double gpa READ getGPA NOTIFY getGPAChanged)
    Q_PROPERTY(QString stream READ getStream NOTIFY getStreamChanged)
    Q_PROPERTY(QString program READ getProgram NOTIFY getProgramChanged)
    Q_PROPERTY(int credit READ getCredit NOTIFY getCreditChanged)
public:
    Session();
    QString getUsername() const;
    void setUsername(const QString &newusername);
    QString getName() const;
    void setName(const QString &newname);
    QChar getCurSem();
    void setCurSem(QChar sem);
    int getCurYear();
    void setCurYear(int ye);
    Student* getStu();
    Manager* getMan();
    void setStu(Student* stu);
    void setMan(Manager* man);
    QString getCurrentSem();
    QVector<QPair<QString,int>>* getPrograms();
    QVector<QPair<QString,int>>* getStreams();
    double getGPA();
    int getCredit();
    QString getStream();
    QString getProgram();

signals:
    void usernameChanged(QString username);
    void nameChanged();
    void getCreditChanged();
    void getCurrentSemChanged();
    void getGPAChanged();
    void getStreamChanged();
    void getProgramChanged();

private:
    QString m_username;
    QString m_name;
    QChar cur_sem;
    int cur_year;
    Student* stu;
    Manager* man;
    QVector<QPair<QString,int>> *programs;
    QVector<QPair<QString,int>> *streams;
};

#endif // SESSION_H
