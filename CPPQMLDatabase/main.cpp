#include <QDebug>
#include <QScreen>
#include <QQmlContext>
#include <QtGui/QGuiApplication>
#include "qtquick2applicationviewer.h"
#include "services.h"
#include "session.h"
#include <QDebug>
#include <QScreen>
#include <QQmlContext>
#include "course.h"
#include "manager.h"
#include "session.h"
#include "programgraph.h"
#include "student.h"
#include "dbconnect.h"
#include "services.h"
#include <QVector>

QVector<Course *> courses;
ProgramGraph* pgraph;
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QtQuick2ApplicationViewer viewer;
    QQmlContext *context = viewer.rootContext();
    Session ses;
    Course found;
    Student student;
    Services svc(context,&ses);
//    svc.doLogin("s3461695","1234");
//    ses.setUsername("s3461695");
//    svc.getCurSem();
//    svc.getUserInformation();
//    svc.getCourseData();
//    svc.enroll("ISYS2077");
    QObject::connect(&ses,SIGNAL(usernameChanged(QString)),&svc,SLOT(changeName(QString)));
    context->setContextProperty("Services",&svc);
    context->setContextProperty("Session",&ses);
    context->setContextProperty("Mainwindow",&viewer);
    context->setContextProperty("Result", &found);
    context->setContextProperty("StudentRes",&student);
    context->setContextProperty("Course",QVariant::fromValue(QList<QObject*>())); // learned courses
    context->setContextProperty("AllCourses",QVariant::fromValue(QList<QObject*>())); // all courses
    context->setContextProperty("Plan",QVariant::fromValue(QList<QObject*>()));
    viewer.setResizeMode(QQuickView::SizeRootObjectToView);
    viewer.setFlags(Qt::FramelessWindowHint);
    viewer.setMainQmlFile(QStringLiteral("qml/CPPQMLDatabase/main.qml"));
    QSize screensize = viewer.screen()->size();
    viewer.setPosition((screensize.width()/2) - (viewer.width()/2),(screensize.height()/2) - (viewer.height()/2));
    viewer.showExpanded();
    return app.exec();
}

