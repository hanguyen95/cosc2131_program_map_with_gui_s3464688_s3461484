#include "session.h"

Session::Session() {
    streams = new QVector<QPair<QString,int>>();
    programs = new QVector<QPair<QString,int>>();
    stu = nullptr;
    man = nullptr;
}

QString Session::getUsername() const {
    return m_username;
}

void Session::setUsername(const QString &newusername) {
    m_username = newusername;
    emit usernameChanged(m_username);
}

QString Session::getName() const {
    return m_name;
}

void Session::setName(const QString &newname) {
    m_name = newname;
}

QChar Session::getCurSem() {
    return cur_sem;
}

void Session::setCurSem(QChar sem) {
    cur_sem = sem;
}

int Session::getCurYear() {
    return cur_year;
}

void Session::setCurYear(int ye) {
    cur_year = ye;
}

Student* Session::getStu() {
    return stu;
}

Manager* Session::getMan() {
    return man;
}

void Session::setStu(Student* stu) {
    Session::stu = stu;
}

void Session::setMan(Manager* man) {
    Session::man = man;
}

QVector<QPair<QString,int>>* Session::getPrograms() {
    return programs;
}

QVector<QPair<QString,int>>* Session::getStreams() {
    return streams;
}

QString Session::getCurrentSem() {
    QString res = QString::number(cur_year) + cur_sem;
    return res;
}

double Session::getGPA() {
    return stu->getGPA();
}

QString Session::getStream() {
    for (int i=0; i < streams->size(); i++) {
        if (streams->at(i).second == stu->getStream())
            return streams->at(i).first;
    }
}

QString Session::getProgram() {
    for (int i=0; i < programs->size(); i++) {
        if (programs->at(i).second == stu->getProgram())
            return programs->at(i).first;
    }
}

int Session::getCredit() {
    return stu->getCredit();
}
