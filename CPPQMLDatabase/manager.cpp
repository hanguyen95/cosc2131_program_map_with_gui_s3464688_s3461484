#include "manager.h"

Manager::Manager(QString id, QString name)
{
    Manager::id = id;
    Manager::name = name;
}

QString Manager::getId() {
    return Manager::id;
}

QString Manager::getName() {
    return Manager::name;
}
