#include "dbconnect.h"

DBConnect::DBConnect()
    {

}

void DBConnect::doConnect(QString driver, QString hostname, QString databasename, QString username, QString password)
{
    m_db = QSqlDatabase::addDatabase(driver);
    m_db.setHostName(hostname);
    m_db.setDatabaseName(databasename);
    m_db.setUserName(username);
    m_db.setPassword(password);
    m_db.setPort(5432);
    m_db.open();
}

void DBConnect::setType(int i) {
    type = i;
}

int DBConnect::getType() const{
    return type;
}

QSqlDatabase DBConnect::con() const
{
    return m_db;
}
