
#ifndef PROGRAMGRAPH_H
#define PROGRAMGRAPH_H

#include <QObject>
#include <QSet>
#include <QtAlgorithms>
#include <QDebug>
#include "course.h"
#include "session.h"
extern QVector<Course *> courses;

typedef struct course_vertex {
    Course *from;
    QVector<QPair<Course *, int>> to;
} course_vertex;
class ProgramGraph {
public:
    ProgramGraph(int n_vertices, int invalid_weight, Session* ses);
    int getWeight(const QString course1, const QString course2) const;

    QVector<course_vertex> getList() const;

    void setWeight(const QString from, const QString to, const int &weight);

    bool hasEdge(const QString from, const QString to);

    void construct_graph();

    /* @param cre planned credit
     * @param check flag if true check offerring, else not check offering
     * @param cursem semester code to check with course's available
     * */
    QVector<Course *> plan_util(int cre, bool check, int cursem);

    Q_INVOKABLE void plan();

    QVector<QVector<Course*>> getPlanned();

    int getN_vertices() const;
    bool planStudy(Course* cou, int cre);

private:
    QVector<course_vertex> listOfVertices;
    const int INVALID_WEIGHT;
    int n_vertices;
    QChar cur_sem;
    int cur_year;
    Session* ses;
    QVector<QVector<Course*>> planned;
};
#endif // PROGRAMGRAPH_H
