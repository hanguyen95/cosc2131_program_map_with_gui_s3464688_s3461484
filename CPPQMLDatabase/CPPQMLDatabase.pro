CONFIG += c++11

# Add more folders to ship with the application, here
folder_01.source = qml/CPPQMLDatabase
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    course.cpp \
    dbconnect.cpp \
    manager.cpp \
    programgraph.cpp \
    services.cpp \
    session.cpp \
    student.cpp

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    course.h \
    dbconnect.h \
    manager.h \
    programgraph.h \
    services.h \
    session.h \
    student.h

QT += sql

OTHER_FILES += \

DISTFILES += \


