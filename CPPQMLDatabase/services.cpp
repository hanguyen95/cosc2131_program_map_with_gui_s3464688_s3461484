#include "services.h"
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QCryptographicHash>
#include <QDateTime>

Services::Services(QQmlContext *ctxt, Session *sess)
{
    m_ctxt = ctxt;
    m_ses = sess;
    m_con = new DBConnect();
}

bool Services::doLogin(QString username, QString password)
{
    if (username.at(0) == 'm' || username.at(0) == 'M') {
        m_con->doConnect("QPSQL","localhost","postgres","admin","12345");
        m_con->setType(1);
    } else if (username.at(0) == 's' || username.at(0) == 'S') {
        m_con->doConnect("QPSQL","localhost","postgres","student","1234");
        m_con->setType(0);
    }
    QSqlQuery query(m_con->con());
    QString sql = (m_con->getType()==1) ? "Select program_map.authenticate_manager(":"Select program_map.authenticate(";
    sql += "'"+username+"','"+password+"');";
    query.exec(sql);
    qDebug() << sql;
    if (query.next()) {
        return query.value(0).toBool();
    } else {
        return false;
    }
}

void Services::getUserInformation() {
    if (m_con->getType() == 0) {//retrieve student data
        QSqlQuery query(m_con->con());
        QString sql = "SELECT sid,name,gpa,semester,completed_credit,attempt,program_id,stream_id FROM program_map.student WHERE sid = '"+m_ses->getUsername()+"';";
        query.prepare(sql);
        query.exec();
        if (query.next()) {
            Student *stu = new Student(query.value(0).toString(),query.value(1).toString(),query.value(2).toDouble(),query.value(3).toInt(),query.value(4).toInt(),query.value(5).toInt(),query.value(6).toInt(),query.value(7).toInt());
            m_ses->setStu(stu);
            retrieveStudentInformation();
        }
    } else if (m_con->getType() == 1) {//retrieve manager data
        QSqlQuery query(m_con->con());
        QString sql = "SELECT id,name FROM program_map.manager WHERE id = '"+m_ses->getUsername()+"';";
        query.prepare(sql);
        query.exec();
        if (query.next()) {
            Manager* man = new Manager(query.value(0).toString(),query.value(1).toString());
            m_ses->setMan(man);
        }
    }
}

void Services::retrieveStudentInformation() {
    QSqlQuery query(m_con->con());
    for (int j=0; j<m_ses->getStu()->getSemester();j++) {
        QString co = QString::number(j+1);
        QString sql = "SELECT course_code,grade FROM program_map.enrollment WHERE sid = '"+m_ses->getUsername()+"' AND std_semester = "+co+";";
        query.prepare(sql);
        query.exec();
        while(query.next()) {
            QVector<QVector<QPair<QString,int>>*> studied_course = *m_ses->getStu()->getStudied_courses();
            studied_course[j]->push_back(qMakePair(query.value(0).toString(),query.value(1).toInt()));
        }
        query.finish();
    }
}

void Services::getAllCourses() {
    QList<QObject *> data;
    for (auto c:courses) {
        data.append(c);
    }
    m_ctxt->setContextProperty("AllCourses",QVariant::fromValue(data));
}

void Services::getCourseData()
{
    QList<QObject *> data;
    QSqlQuery query(m_con->con());
    query.prepare("SELECT code,description,title,credit_points,available,year,prerequisite,stream,rating,program_id FROM program_map.course;");
    query.exec();
    while (query.next()) {
        Course *c = new Course(query.value(0).toString(),query.value(1).toString(),query.value(2).toString(),query.value(3).toInt(),query.value(4).toInt(),query.value(5).toInt(),query.value(6).toString(),query.value(7).toInt(),query.value(8).toInt(),query.value(9).toInt(),m_ses);
        if (m_ses->getStu() != nullptr) {
            for (int j=0; j<m_ses->getStu()->getStudied_courses()->size();j++) {
                for (int i=0; i< m_ses->getStu()->getStudied_courses()->at(j)->size();i++) {
                    if (c->getCode()== m_ses->getStu()->getStudied_courses()->at(j)->at(i).first) {
                        if (m_ses->getStu()->getStudied_courses()->at(j)->at(i).second >= 50) {
                            c->setPlanned(true);
                            c->setStudied(true);
                            break;
                        } else if (m_ses->getStu()->getStudied_courses()->at(j)->at(i).second == -1) {
                            c->setPlanned(true);
                            break;
                        } else {
                            break;
                        }
                    }
                }
            }
            if (c->isStudied() || c->isPlanned())
                data.append(c);
        }
        courses.push_back(c);
    }
    for (int i=0; i<courses.size(); i++) {
        query.finish();
        QString sql = "SELECT SUM(fail_percent) FROM program_graph.course_history WHERE course_code ='";
        sql += courses.at(i)->getCode() + "' GROUP BY semester ORDER BY semester DESC LIMIT 3;";
        query.prepare(sql);
        query.exec();
        if (query.next()) {
            double temp = query.value(0).toDouble()/3;
            courses.at(i)->setFailrate(temp);
        }
        query.finish();
        QString sql1 = "SELECT SUM(avg) FROM program_graph.course_history WHERE course_code ='";
        sql1 += courses.at(i)->getCode() + "' GROUP BY semester ORDER BY semester DESC LIMIT 3;";
        query.prepare(sql1);
        query.exec();
        if (query.next()) {
            double temp = query.value(0).toDouble()/3;
            courses.at(i)->setFailrate(temp);
        }
    }

    if (m_ses->getStu() != nullptr)
        m_ctxt->setContextProperty("Course",QVariant::fromValue(data));
}

void Services::planning() {
    pgraph = new ProgramGraph(courses.size(),-1,m_ses);
    pgraph->construct_graph();
    pgraph->plan();
    QVector<QVector<Course*>> planning = pgraph->getPlanned();
    QList<QObject*> data;
    for (int i=0; i<planning.size(); i++) {
        for (int j=0; j<planning.at(i).size(); j++) {
            data.append(planning.at(i).at(j));
        }
    }
    m_ctxt->setContextProperty("Plan",QVariant::fromValue(data));
//    for(int i=0; i< planning.size(); i++) {
//        qDebug() << "Sem " << i;
//        for (int j=0; j<planning.at(i).size();j++) {
//            qDebug() << planning.at(i).at(j)->getCode();
//        }
//    }
}

void Services::changeName(QString username)
{
    QSqlQuery query(m_con->con());
    QString sql;
    if (m_con->getType() == 0)
        sql = "Select name from program_map.student where sid = '"+username+"';";
    else
        sql = "Select name from program_map.manager where id = '"+username+"';";
    query.prepare(sql);
    query.bindValue(0,username);
    query.exec();
    if(query.next())m_ses->setName(query.value("Name").toString());
}

QString Services::getUserRole()
{
    if (m_ses->getUsername().at(0) == 'm' || m_ses->getUsername().at(0) == 'M') {
        return "Manager";
    } else {
        return "Student";
    }
}

void Services::getCurSem() {
    QSqlQuery query(m_con->con());
    QString sql = "Select program_map.get_current_sem();";
    query.prepare(sql);
    query.exec();
    if (query.next()) {
        QString tmp = query.value(0).toString();
        m_ses->setCurSem(tmp.at(4));
        QStringRef stmp(&tmp,0,4);
        m_ses->setCurYear(stmp.toInt());
    }
}

void Services::getStreamProgramData() {
    QSqlQuery query(m_con->con());
    query.prepare("SELECT name,id FROM program_map.stream;");
    query.exec();
    QVector<QPair<QString,int>> *streams = m_ses->getStreams();
    while (query.next()) {
        streams->push_back(qMakePair(query.value(0).toString(),query.value(1).toInt()));
    }
    query.finish();
    query.prepare("SELECT name,id FROM program_map.programs;");
    query.exec();
    QVector<QPair<QString,int>> *programs = m_ses->getPrograms();
    while (query.next()) {
        programs->push_back(qMakePair(query.value(0).toString(),query.value(1).toInt()));
    }
}

void Services::find_course(QString code) {
    for (auto course: courses) {
        if (course->getCode() == code) {
            m_ctxt->setContextProperty("Result", course);
            return;
        }
    }
}

void Services::resetData() {
    qDebug() << "abc";
    if (m_ses->getStu() != nullptr) {
        for (int i=0; i<m_ses->getStu()->getStudied_courses()->size();i++) {
            if (m_ses->getStu()->getStudied_courses()->at(i) != nullptr)
                delete m_ses->getStu()->getStudied_courses()->at(i);
        }
        delete m_ses->getStu()->getStudied_courses();       
        delete m_ses->getStu();
        m_ses->setStu(nullptr);
    }
    if (m_ses->getMan() != nullptr) {
        delete m_ses->getMan();
        m_ses->setMan(nullptr);
    }
    for (auto course: courses) {
        delete course;
    }
    courses.clear();
    if (pgraph!=nullptr) {
        pgraph->getPlanned().clear();
        pgraph->getList().clear();
        delete pgraph;
        pgraph = nullptr;
    }
}

void Services::enroll(QString code) {
    if (m_ses->getStu() == nullptr) return;
    QSqlQuery query(m_con->con());
    QString sql = "INSERT INTO program_map.enrollment (course_code,sid,semester) VALUES ('";
    sql += code +"','" +m_ses->getStu()->getId()+"','"+m_ses->getCurrentSem()+"');";
    query.prepare(sql);
    if (query.exec()) {
        qDebug() << "Succeed";
        if (m_ses->getStu() != nullptr) {
            for (int i=0; i<m_ses->getStu()->getStudied_courses()->size();i++) {
                if (m_ses->getStu()->getStudied_courses()->at(i) != nullptr)
                    delete m_ses->getStu()->getStudied_courses()->at(i);
            }
            delete m_ses->getStu()->getStudied_courses();
            QVector<QVector<QPair<QString,int>>*>* studied = new QVector<QVector<QPair<QString,int>>*>();
            m_ses->getStu()->setStudied_courses(studied);
            retrieveStudentInformation();
        }
        for (auto course: courses) {
            delete course;
        }
        courses.clear();
        if (pgraph!=nullptr) {
            pgraph->getPlanned().clear();
            pgraph->getList().clear();
            delete pgraph;
        }
        getCourseData();
        getAllCourses();
        planning();
    } else {
        qDebug() << "Not succeeded";
    }
}

void Services::find_student(QString sid) {
    QSqlQuery query(m_con->con());
    QString sql = "SELECT sid,name,gpa,semester,completed_credit,attempt,program_id,stream_id FROM program_map.student WHERE sid = '"+sid+"';";
    query.prepare(sql);
    query.exec();
    if (query.next()) {
        Student *stu = new Student(query.value(0).toString(),query.value(1).toString(),query.value(2).toDouble(),query.value(3).toInt(),query.value(4).toInt(),query.value(5).toInt(),query.value(6).toInt(),query.value(7).toInt());
        m_ses->setStu(stu);
        retrieveStudentInformation();
        m_ctxt->setContextProperty("StudentRes",stu);
    }
}

/**Tokenizing the raw prerequisite and put it in a vector to translate to
postfix type
  *@param prereq raw prerequisite
  */
QVector<QString> Services::tokenize_prereq(QString prereq) {
    QVector<QString> res;
    // String used to find number
    QString digit = "0123456789";

    // Appending token into res
    for (int i=0; i<prereq.size();i++) {
        if (prereq.at(i) == '(' || prereq.at(i) == ')') {
            res.push_back(QString(1,prereq.at(i)));
        } else if (prereq.at(i) == 'C' || prereq.at(i) == 'I' || prereq.at(i) == 'M') {
            // Search for course code
            res.push_back(prereq.mid(i,8));
            i+=7;
        } else if (prereq.at(i) == 'A') {
            // AND operator
            res.push_back("*");
            i+=2;
        } else if (prereq.at(i) == 'O') {
            // OR operator
            res.push_back("+");
            i+=1;
        } else if (digit.indexOf(prereq.at(i)) != -1) {
            // Search for credit-type prerequisite
            if (digit.indexOf(prereq.at(i+2)) != -1) {
                res.push_back(prereq.mid(i,3));
                i+=2;
            } else {
                res.push_back(prereq.mid(i,2));
                i+=1;
            }
        }
    }
    return res;
}

/** Check if precedence level of token1 is higher than token2 or not
  * @param token1
  * @param token2
  */
bool Services::compare_precedence(QString token1, QString token2) {
    // "(" is less than "AND" and "OR", "AND" and "OR" are the same
    if (token1 == "(") {
        if (token2 == "(") {
            return true;
        } else if (token2 == "AND" || token2 == "OR"){
            return false;
        }
    } if (token1 == "AND" || token1 == "OR") {
        return true;
    }
}

/** convert raw prerequisite from infix to postfix
  * @param prereq raw prerequisite in infix mode
  */
QString Services::convertPrereq(QString prereq) {
    QString res;
    if (prereq == "No prerequisite")
      res = "/";
    else if(prereq.size() == 8) {
      // only one course prereq
      res = prereq;
    }
    else if(prereq.size() == 10) {
      // credit-type course prereq - 2 digits
      res = prereq.mid(0,2);
    }
    else if (prereq.size() == 11) {
      // credit-type course prereq - 3 digits
      res = prereq.mid(0,3);
    }
    else {// multi-prerequisite type
        // initialize
        QVector<QString> elem = tokenize_prereq(prereq);
        QStack<QString> tmp_sta;
        QVector<QString> vres;

        // reference: http://interactivepython.org/runestone/static/pythonds/BasicDS/InfixPrefixandPostfixExpressions.html
        for (int i=0;i<elem.size();i++) {
            if (elem.at(i).size()==8 || elem.at(i).size()==2 || elem.at(i).size()==3) {
                // course code or number
                vres.push_back(elem.at(i));
            }
            else if (elem.at(i) == "(") {
                tmp_sta.push("(");
            }
            else if (elem.at(i) == ")") {
                QString tmp = tmp_sta.top();
                tmp_sta.pop();
                while (tmp != "(") {
                    vres.push_back(tmp);
                    tmp = tmp_sta.top();
                    tmp_sta.pop();
                }
            }
            else {
                while (!tmp_sta.empty() && compare_precedence(tmp_sta.top(),elem.at(i))) {
                    vres.push_back(tmp_sta.top());
                    tmp_sta.pop();
                }
                tmp_sta.push(elem.at(i));
            }
        }

        while (!tmp_sta.empty()) {
            vres.push_back(tmp_sta.top());
            tmp_sta.pop();
        }

        // reconstruct prerequisite in postfix
        for (int i=0; i<vres.size(); i++) {
            res += vres.at(i) + ",";
        }
        res = res.mid(0,res.size()-1);
    }
    return res;
}

bool Services::create_course(QString co, QString ti, QString pr, QString st, QString ye, QString de, QString pre, int avai, QString ra, QString cre) {
    if (m_con->getType() == 0)
        return false;
    QSqlQuery query(m_con->con());
    pre = convertPrereq(pre);
    int tmp = 0;
    for (int i=0; i<m_ses->getPrograms()->size(); i++) {
        if (m_ses->getPrograms()->at(i).first == pr) {
            tmp = m_ses->getPrograms()->at(i).second;
            break;
        }
    }
    int tmp1 = 0;
    for (int i=0; i<m_ses->getStreams()->size(); i++) {
        if (m_ses->getStreams()->at(i).first == st) {
            tmp1 = m_ses->getStreams()->at(i).second;
            break;
        }
    }
    QString sql = "INSERT INTO program_map.course (code,title,program_id,stream,year,description,prerequisite,available,rating,credit_points) ";
    sql += "VALUES ('" + co + "','" + ti + "','" + QString::number(tmp) + "'," + QString::number(tmp1) + ",";
    sql += ye + ",'" + de + "','" + pre + "'," + QString::number(avai) + ",";
    sql += ra + "," + cre + ");";
    query.prepare(sql);
    if (query.exec()) {
        for (auto course: courses) {
            delete course;
        }
        courses.clear();
        getCourseData();
        getAllCourses();
        return true;
    }
    else
        return false;
}

QStringList Services::getProgramList() {
    QSqlQuery query(m_con->con());
    QStringList data;
    data.append("Choose Program");
    QString sql = "SELECT name FROM program_map.programs;";
    query.prepare(sql);
    query.exec();
    while (query.next()) {
        data.append(query.value(0).toString());
    }
    return data;
}

QStringList Services::getStreamList(QString program) {
    QStringList data;
    data.append("Choose Stream");
    if (program == "Choose Program") return data;
    int tmp=0;
    for (int i=0; i<m_ses->getPrograms()->size();i++) {
        if (m_ses->getPrograms()->at(i).first == program) {
            tmp = m_ses->getPrograms()->at(i).second;
            break;
        }
    }
    QSqlQuery query(m_con->con());
    QString sql = "SELECT name FROM program_map.stream WHERE program_id = "+QString::number(tmp)+";";
    query.prepare(sql);
    query.exec();
    while (query.next()) {
        data.append(query.value(0).toString());
    }
    return data;
}
