#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>

class Manager : public QObject
{
        Q_OBJECT
        Q_PROPERTY(QString m_id READ getId)
        Q_PROPERTY(QString m_name READ getName)
public:
    Manager(QString id, QString name);
    QString getId();
    QString getName();

private:
    QString id;
    QString name;
};

#endif // MANAGER_H
