#include "course.h"

Course::Course(){}

Course::Course(QString code, QString desc, QString title, int credit, int avai, int year, QString prerequisite, int stream, int rating, int program, Session* ses) {
    Course::code = code;
    Course::desc = desc;
    Course::title = title;
    Course::credit = credit;
    Course::avai = avai;
    Course::year = year;
    Course::prereq = prerequisite;
    Course::stream = stream;
    Course::rating = rating;
    Course::program = program;
    Course::priority = 0;
    Course::is_studied = false;
    Course::is_planned = false;
    Course::ses = ses;
}

QString Course::getStatus() {
    QString res = "";
    if(isStudied()) {
        res = "Completed";
    }
    else if (isPlanned()) {
        res = "In Progress";
    }
    return res;
}

QString Course::getCode() const {
    return Course::code;
}

QString Course::getDesc() const {
    return Course::desc;
}

QString Course::getTitle() const {
    return Course::title;
}

int Course::getCredit() const {
    return Course::credit;
}

int Course::getAvai() const {
    return Course::avai;
}

int Course::getYear() const {
    return Course::year;
}

QString Course::getPrereq() const {
    return Course::prereq;
}

int Course::getStream() const {
    return Course::stream;
}

int Course::getRating() const {
    return Course::rating;
}

int Course::getProgram() const {
    return Course::program;
}

int Course::getPriority() const {
    return Course::priority;
}

void Course::setCode(QString code) {
    Course::code = code;
}

void Course::setDesc(QString desc) {
    Course::desc = desc;
}

void Course::setTitle(QString title) {
    Course::title = title;
}

void Course::setCredit(int credit) {
    Course::credit = credit;
}

void Course::setAvai(int avai) {
    Course::avai = avai;
}

void Course::setYear(int year) {
    Course::year = year;
}

void Course::setPrereq(QString prerequisite) {
    Course::prereq = prerequisite;
}

void Course::setStream(int stream) {
    Course::stream = stream;
}

void Course::setRating(int rating) {
    Course::rating = rating;
}

void Course::setProgram(int program) {
    Course::program = program;
}

void Course::setStudied(bool is_studied) {
    Course::is_studied = is_studied;
}

void Course::setPlanned(bool is_planned) {
    Course::is_planned = is_planned;
}

bool Course::isStudied() {
    return is_studied;
}

bool Course::isPlanned() {
    return is_planned;
}

void Course::calcPriority() {
    if (rating == 1)
        priority += 10;
    else if (rating == 2)
        priority += 5;
    else
        priority += 1;
    if (failrate <= 0.1)
        priority += 3;
    else if (failrate <= 0.25)
        priority += 2;
    else if (failrate <= 0.4)
        priority += 1;
    priority += qFloor(avggpa);
    for (int i = ses->getStu()->getStudied_courses()->size()-1; i >= 0 ; i--) {
        bool found = false;
        for (int j=0; j < ses->getStu()->getStudied_courses()->at(i)->size();j++) {
            if (ses->getStu()->getStudied_courses()->at(i)->at(j).first == code) {
                if (ses->getStu()->getStudied_courses()->at(i)->at(j).second >= 50 || ses->getStu()->getStudied_courses()->at(i)->at(j).second == -1)
                    priority += 10;
                found = true;
                break;
            }
        }
        if (found)
            break;
    }
}

QString Course::getConvertedPrereq() {
    QString res;
    if (prereq == "/")
        res = "No prerequisite.";
    else if (prereq.size() == 2 || prereq.size() == 3)
        res = prereq + " credits.";
    else if (prereq.size() == 8)
        res = prereq +".";
    else {
        QStringList elem = prereq.split(",");
        QStack<QString> tmp_sta;

        for (int i=0; i<elem.size();i++) {
            if (elem.at(i).size() == 1) {
                QString le = tmp_sta.top();
                tmp_sta.pop();
                if (le.size() == 2 || le.size() == 3)
                    le += " credits";
                else if (le.size() > 8)
                    le = "("+le+")";
                QString ri = tmp_sta.top();
                tmp_sta.pop();
                if (ri.size() == 2 || ri.size() == 3)
                    ri += " credits";
                else if (ri.size() > 8)
                    ri = "("+ri+")";
                if (elem.at(i) == "*")
                    tmp_sta.push(ri+" AND "+le);
                else
                    tmp_sta.push(ri+" OR "+le);
            } else
                tmp_sta.push(elem.at(i));
        }
        res = tmp_sta.top()+".";
    }
    return res;
}

bool Course::canStudy() {
    if (isStudied())
        return false;
    int tmp = 0;
    if (ses->getCurSem() == 'A') tmp = 1;
    else if (ses->getCurSem() == 'B') tmp = 2;
    else tmp = 4;
    int tmp1 = tmp & avai;
    if (tmp1 != tmp)
        return false;
    if (prereq.size() == 1) {
        return true;
    } else if (prereq.size() == 8) {
        for (int i = 0; i < ses->getStu()->getStudied_courses()->size(); i++) {
            for (int j=0; j < ses->getStu()->getStudied_courses()->at(i)->size();j++) {
                if (ses->getStu()->getStudied_courses()->at(i)->at(j).first == prereq)
                    return ses->getStu()->getStudied_courses()->at(i)->at(j).second == -1 || ses->getStu()->getStudied_courses()->at(i)->at(j).second >= 50;
            }
        }
    } else if (prereq.size() > 8) {
        QStringList elem = prereq.split(",");
        QStack<QString> tmp_sta;
        for (int i = 0; i < elem.size(); i++) {
            if (elem.at(i).size() != 1) {
                tmp_sta.push(elem.at(i));
            } else {
                QString le = tmp_sta.top();
                tmp_sta.pop();
                bool bool_l = false;
                if (le.size() == 8) { // if it's a course
                    for (int j = 0; j < ses->getStu()->getStudied_courses()->size(); j++) {
                        for (int k=0; k < ses->getStu()->getStudied_courses()->at(j)->size(); k++) {
                            if (ses->getStu()->getStudied_courses()->at(j)->at(k).first == le) {
                                bool_l = ses->getStu()->getStudied_courses()->at(j)->at(k).second == -1 || ses->getStu()->getStudied_courses()->at(j)->at(k).second >=50;
                                break;
                            }
                        }
                    }
                } else if (le.size() == 5) { // if the right element is a result or processed expression
                    bool_l = le == "true1";
                } else { // it's credit point prerequisite
                    int credit = le.toInt();
                    bool_l = ses->getStu()->getCredit() >= credit;
                }

                QString ri = tmp_sta.top();
                tmp_sta.pop();
                bool bool_r = false;
                if (ri.size() == 8) { // if it's a course
                    for (int j = 0; j < ses->getStu()->getStudied_courses()->size(); j++) {
                        for (int k=0; k < ses->getStu()->getStudied_courses()->at(j)->size(); k++) {
                            if (ses->getStu()->getStudied_courses()->at(j)->at(k).first == ri) {
                                bool_r = ses->getStu()->getStudied_courses()->at(j)->at(k).second == -1 || ses->getStu()->getStudied_courses()->at(j)->at(k).second >= 50;
                                break;
                            }
                        }
                    }
                } else if (ri.size() == 5) { // if the right element is a result or processed expression
                    bool_r = ri == "true1";
                } else { // it's credit point prerequisite
                    int credit = ri.toInt();
                    bool_r = ses->getStu()->getCredit() >= credit;
                }

                if (elem.at(i) == "+")
                    tmp_sta.push((bool_r || bool_l) ? "true1" : "false");
                else
                    tmp_sta.push((bool_r && bool_l) ? "true1" : "false");
            }
        }
        return tmp_sta.top() == "true1";
    } else if (prereq.size() == 2 || prereq.size() == 3)
        return ses->getStu()->getCredit() >= prereq.toInt();
    return false;
}

QString Course::getGrade() const{
    QString res;
    for (int i=ses->getStu()->getStudied_courses()->size()-1; i>=0; i--) {
        for(int j = 0; j<ses->getStu()->getStudied_courses()->at(i)->size(); j++) {
            if(ses->getStu()->getStudied_courses()->at(i)->at(j).first == code) {
                if(ses->getStu()->getStudied_courses()->at(i)->at(j).second == -1) {
                    res = "";
                }
                else if(ses->getStu()->getStudied_courses()->at(i)->at(j).second < 50) {
                    res = "Failed";
                }
                else if(ses->getStu()->getStudied_courses()->at(i)->at(j).second < 60) {
                    res = "PA";
                }
                else if(ses->getStu()->getStudied_courses()->at(i)->at(j).second < 70) {
                    res = "CR";
                }
                else if(ses->getStu()->getStudied_courses()->at(i)->at(j).second < 80) {
                    res = "DI";
                }
                else {
                    res = "HD";
                }
                return res;
            }
        }
    }
}

QString Course::getCStream() {
    for(int i=0; i < ses->getStreams()->size(); i++) {
        if (ses->getStreams()->at(i).second == stream)
            return ses->getStreams()->at(i).first;
    }
}

QString Course::getCProgram() {
    for (int i=0; i < ses->getPrograms()->size(); i++) {
        if (ses->getPrograms()->at(i).second == program)
            return ses->getPrograms()->at(i).first;
    }
}

void Course::setFailrate(double rate) {
    failrate = rate;
}

void Course::setAvggpa(double avg) {
    avggpa = avg;
}

double Course::getAvggpa() {
    return avggpa;
}

double Course::getFailrate() {
    return failrate;
}
