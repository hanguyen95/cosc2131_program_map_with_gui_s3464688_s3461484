#include "programgraph.h"

    ProgramGraph::ProgramGraph(int n_vertic, int invalid_weight, Session* ses) : n_vertices{n_vertic}, INVALID_WEIGHT{invalid_weight}, ses{ses} {
        cur_sem = ses->getCurSem();
        cur_year = ses->getCurYear();
        listOfVertices = QVector<course_vertex>(n_vertices);
        for (int i = 0; i < n_vertices; i++) {
            listOfVertices[i].from = courses.at(i);
        }
    }

    int ProgramGraph::getWeight(const QString course1, const QString course2) const {
        for (int i = 0; i < listOfVertices.size(); i++) {
            if (listOfVertices.at(i).from->getCode().compare(course1) == 0) {
                for (int j = 0; j < listOfVertices.at(i).to.size(); j++) {
                    if (listOfVertices.at(i).to.at(j).first->getCode().compare(course2) == 0)
                        return listOfVertices.at(i).to.at(j).second;
                }
                return INVALID_WEIGHT;
            }
        }
        return INVALID_WEIGHT;
    }

    QVector<course_vertex> ProgramGraph::getList() const {
        return listOfVertices;
    }

    void ProgramGraph::setWeight(const QString from, const QString to, const int &weight) {
        for (int i = 0; i < listOfVertices.size(); i++) {
            if (listOfVertices.at(i).from->getCode() == from) {
                for (int j=0; j < listOfVertices[i].to.size(); j++) {
                    if (listOfVertices[i].to[j].first->getCode() == to) {
                        listOfVertices[i].to[j].second = weight;
                        return;
                    }
                }
                for (auto course:courses) {
                    if (course->getCode() == to) {
                        listOfVertices[i].to.push_back(qMakePair(course,weight));
                    }
                }
            }
        }
    }

bool ProgramGraph::hasEdge(const QString from, const QString to) {
        return getWeight(from, to) != INVALID_WEIGHT;
    }

    void ProgramGraph::construct_graph() {
        // first loop for from
        for (int i = 0; i < listOfVertices.size(); i++) {
            // second loop for setting to and each weight

            for (int j = 0; j < listOfVertices.size(); j++) {
                if (j != i) {
                    int tmp = ses->getStu()->getStream() & courses[j]->getStream();
                    if (tmp == 0 && (courses[j]->getPrereq().indexOf(courses[i]->getCode()) != -1 ||
                            courses[j]->getPrereq() == "/" || courses[j]->getPrereq().size() == 2
                        || courses[j]->getPrereq().size() == 3) && courses[i]->getPrereq().indexOf(courses[j]->getCode()) == -1) { // if the course is an elective
                        setWeight(courses[i]->getCode(), courses[j]->getCode(), 1);
                    }
                    else { // if the to course is a core course
                        if (courses[j]->getPrereq().indexOf(
                                courses[i]->getCode()) !=
                            -1) { // if the to course has prerequisite as the from course
                            setWeight(courses[i]->getCode(), courses[j]->getCode(), 12);
                        }
                        else if ((courses[j]->getPrereq() == "/" &&
                                  courses[i]->getPrereq().indexOf(courses[j]->getCode()) == -1) ||
                                 courses[j]->getPrereq().size() == 2 || courses[j]->getPrereq().size() == 3)
                            setWeight(courses[i]->getCode(), courses[j]->getCode(), 9);
                    }
                }
            }
        }
    }

    // function supporter to sort second value of 2 pairs
    static bool cmpfunc(const QPair<Course *, int> &pair1, const QPair<Course *, int> &pair2) {
        return (pair1.second > pair2.second);
    };

    QVector<Course *> ProgramGraph::plan_util(int cre, bool check, int cursem) {
        int cps = ses->getStu()->getCouPerSem();
        // for new student without finishing any course
        if (cre == 0) {
            QVector<QPair<Course *, int>> res;
            for (auto course: courses) {
                int tmp = ses->getStu()->getStream() & course->getStream();
                // find the core course with out prerequisites
                if (tmp != 0 && course->getPrereq() == "/") {
                    res.push_back(qMakePair(course, 0));
                }
            }

            // combine with the priority score of each course
            for (QVector<QPair<Course *, int>>::iterator it = res.begin(); it != res.end(); ++it) {
                it->second = it->first->getPriority();
            }

            // sort the set in descending value of priority
            qSort(res.begin(), res.end(), cmpfunc);

            // return vector of courses with number equals students's preferred courses per semester
            QVector<Course *> plan;
            QVector<QPair<Course *, int>>::iterator it_end;
            if (res.end() < res.begin() + cps) it_end = res.end();
            else it_end = res.begin() + cps;
            for (QVector<QPair<Course *, int>>::iterator it = res.begin(); it != it_end; ++it) {
                plan.push_back(it->first);
                it->first->setPlanned(true); // change the flag to planned
            }
            return plan;
        }
        else { // for current student
            QSet<QPair<Course *, int>> res; // set of course codes which are the result after planning

            // loop through the courses and process the prerequisites
            for (int i = 0; i < listOfVertices.size(); i++) {
                if (listOfVertices[i].from->isPlanned()) {
                    for (auto v : listOfVertices[i].to) {
                        if (v.first->isPlanned()) // skip finished courses
                            continue;
                        QString prereq = v.first->getPrereq();
                        //split the prerequisites string into an array by comma ','
                        QStringList elem = prereq.split(",");

                        if (elem.size() == 1) {
                            // check if the course has no prerequisite, which is '/'
                            if (elem.at(0).size() == 1) {
                                res.insert(qMakePair(v.first, 0));
                            }
                                // check if the course has 1 prerequisite
                            else if (elem.at(0).size() == 8) {
                                bool can = false;
                                for (auto course:courses) {
                                    if (course->isPlanned() && course->getCode() == elem.at(0)) {
                                        res.insert(qMakePair(v.first, v.second));
                                        can = true;
                                        break;
                                    }
                                }
                                if (!can) {
                                    for (QSet<QPair<Course *, int>>::iterator it = res.begin(); it != res.end(); it++) {
                                        if (v.first == it->first) {
                                            res.erase(it);
                                            break;
                                        }
                                    }
                                }
                            }
                            else { // it's credit point prerequisite
                                int credit = elem.at(0).toInt();
                                if (cre >= credit) {
                                    res.insert(qMakePair(v.first, 3));
                                }
                            }
                            continue;
                        }
                        // using stack to process prerequisite since in database it is stored in the style of rpn
                        QStack<QString> tmp_sta;
                        bool can = true; // boolean to check if student is able to learn a course
                        int weight = 3; // initial weight for a prerequisite
                        for (int j = 0; j < elem.size(); j++) {
                            // if it's a '+' or '-'
                            if (elem.at(j).size() == 1) {
                                QString ri = tmp_sta.top();
                                tmp_sta.pop();

                                bool bool_r = false; // check if the right element is achievable
                                if (ri.size() == 8) { // if it's a course
                                    for (auto course: courses) {
                                        if (course->getCode() == ri && course->isPlanned()) {
                                            bool_r = true;
                                            break;
                                        }
                                    }
                                } else if (ri.size() == 5) { // if the right element is a result or processed expression
                                    bool_r = ri == "true1";
                                } else { // it's credit point prerequisite
                                    int credit = ri.toInt();
                                    bool_r = cre >= credit;
                                }

                                // the left element
                                QString le = tmp_sta.top();
                                tmp_sta.pop();
                                bool bool_l = false;
                                if (le.size() == 8) {
                                    for (auto course: courses) {
                                        if (course->getCode() == le && course->isPlanned()) {
                                            bool_l = true;
                                            break;
                                        }
                                    }
                                } else if (le.size() == 5) {
                                    bool_l = le == "true1";
                                } else {
                                    int credit = le.toInt();
                                    bool_l = cre >= credit;
                                }

                                // '+' is represented for OR, '*' for AND
                                if (elem.at(j) == "+") {
                                    if (bool_l || bool_r) tmp_sta.push("true1");
                                    else tmp_sta.push("false");
                                }
                                else {
                                    if (bool_l && bool_r){
                                        tmp_sta.push("true1");
                                        weight += 3;
                                    } else {
                                        tmp_sta.push("false");
                                        if (!bool_l && le.size() == 8) {
                                            bool found = false;
                                            for (QSet<QPair<Course *, int>>::iterator it = res.begin();it != res.end(); it++) {
                                                if (it->first->getCode() == le) {
                                                    res.insert(qMakePair(it->first, it->second+3));
                                                    found = true;
                                                    res.erase(it);
                                                    break;
                                                }
                                            }
                                            if (!found){
                                                for (auto course: courses) {
                                                    if (course->getCode() == le) {
                                                        res.insert(qMakePair(course,getWeight(v.first->getCode(),le)));
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (!bool_r && ri.size() == 8) {
                                            bool found = false;
                                            for (QSet<QPair<Course *, int>>::iterator it = res.begin();it != res.end(); it++) {
                                                if (it->first->getCode() == ri) {
                                                    res.insert(qMakePair(it->first, it->second+3));
                                                    found = true;
                                                    res.erase(it);
                                                    break;
                                                }
                                            }
                                            if (!found){
                                                for (auto course: courses) {
                                                    if (course->getCode() == ri) {
                                                        res.insert(qMakePair(course,getWeight(v.first->getCode(),ri)));
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                tmp_sta.push(elem.at(j));
                            }
                        }
                        // store the result of current expression for the next one
                        can = tmp_sta.top() == "true1";
                        if (can)
                            res.insert(qMakePair(v.first, weight));
                    }
                }
            }
            QVector<QPair<Course*,int>> vres;
            // if that course can not be learned yet and is in the set, erase it copy set to vector
            for (QSet<QPair<Course *, int>>::iterator it = res.begin(); it != res.end(); it++) {
                if (check) {
                    int tmp = it->first->getAvai() & cursem;
                    if (tmp == 0) {
                        res.erase(it);
                        continue;
                    }
                }
                if (!planStudy(it->first,cre)) {
                    res.erase(it);
                    continue;
                }
                vres.push_back(qMakePair(it->first,it->second));
            }
            // combine with the priority score of each course
            for (QVector<QPair<Course *, int>>::iterator it = vres.begin(); it != vres.end(); ++it) {
                it->second += it->first->getPriority();
            }
            // sort the set in descending value of priority
            qSort(vres.begin(), vres.end(), cmpfunc);
            // return vector of courses with number equals students's preferred courses per semester
            QVector<Course *> plan;
            for (int i=0; i<vres.size();i++) {
                if (i==cps) break;
                plan.push_back(vres.at(i).first);
                vres.at(i).first->setPlanned(true); // set flag to planned courses
            }
            return plan;
        }
    }

    void ProgramGraph::plan() {
        int planned_cred = 0;
        for (int i=0; i < ses->getStu()->getStudied_courses()->size(); i++) {
            QVector<Course*> tmp;
            for (int j=0; j < ses->getStu()->getStudied_courses()->at(i)->size(); j++){
                for (auto course:courses) {
                    if (course->getCode() == ses->getStu()->getStudied_courses()->at(i)->at(j).first) {
                        if (ses->getStu()->getStudied_courses()->at(i)->at(j).second >= 50 || ses->getStu()->getStudied_courses()->at(i)->at(j).second == -1)
                            tmp.push_back(course);
                        break;
                    }
                }
            }
            if (tmp.size() > 0)
                planned.push_back(tmp);
        }

        for (auto course:courses) {
            course->calcPriority();
            if (course->isPlanned())
                planned_cred += course->getCredit();
        }
        int count = 0; //number of semester that has offerring plan
        int cursem = 0;
        if (cur_sem == 'A') {
            count = 3;
            cursem = 1;
        }
        else if (cur_sem == 'B') {
            count = 2;
            cursem = 2;
        }
        else {
            count = 1;
            cursem = 4;
        }
        while (planned_cred < 264) {
            QVector<Course*> temp = plan_util(planned_cred, count!=0, cursem);
            for (auto c : temp) {
                planned_cred += c->getCredit();
            }
            if (temp.size() > 0)
                planned.push_back(temp);
            if (count != 0) {
                count--;
                cursem *= 2;
            }
        }
        for (auto course:courses)
            course->setPlanned(false);
        for (int i=0; i < ses->getStu()->getStudied_courses()->size(); i++) {
            for (int j=0; j < ses->getStu()->getStudied_courses()->at(i)->size(); j++){
                for (auto course:courses) {
                    if (course->getCode() == ses->getStu()->getStudied_courses()->at(i)->at(j).first) {
                        if (ses->getStu()->getStudied_courses()->at(i)->at(j).second >= 50 || ses->getStu()->getStudied_courses()->at(i)->at(j).second == -1)
                            course->setPlanned(true);
                        break;
                    }
                }
            }
        }
    }

    QVector<QVector<Course*>> ProgramGraph::getPlanned() {
        return planned;
    }

    int ProgramGraph::getN_vertices() const{
        return n_vertices;
    }

    bool ProgramGraph::planStudy(Course* cou, int cre) {
        QString prereq = cou->getPrereq();
        if (prereq.size() == 1)
            return true;
        else if (prereq.size() == 8) {
            for (auto course:courses) {
                if (course->getCode() == prereq) {
                    if (course->isPlanned())
                        return true;
                    return false;
                }
            }
        } else if (prereq.size() == 2 || prereq.size() == 3) {
            return cre >= prereq.toInt();
        } else {
            QStringList elem = prereq.split(",");
            QStack<QString> tmp_sta;
            for (int i = 0; i < elem.size(); i++) {
                if (elem.at(i).size() != 1) {
                    tmp_sta.push(elem.at(i));
                } else {
                    QString le = tmp_sta.top();
                    tmp_sta.pop();
                    bool bool_l = false;
                    if (le.size() == 8) { // if it's a course
                        for (auto course:courses) {
                            if (course->getCode() == le) {
                                bool_l = course->isPlanned();
                                break;
                            }
                        }
                    } else if (le.size() == 5) { // if the right element is a result or processed expression
                        bool_l = le == "true1";
                    } else { // it's credit point prerequisite
                        int credit = le.toInt();
                        bool_l = cre >= credit;
                    }

                    QString ri = tmp_sta.top();
                    tmp_sta.pop();
                    bool bool_r = false;
                    if (ri.size() == 8) { // if it's a course
                        for (auto course: courses) {
                            if (course->getCode() == ri) {
                                bool_r = course->isPlanned();
                                break;
                            }
                        }
                    } else if (ri.size() == 5) { // if the right element is a result or processed expression
                        bool_r = ri == "true1";
                    } else { // it's credit point prerequisite
                        int credit = ri.toInt();
                        bool_r = cre >= credit;
                    }

                    if (elem.at(i) == "+")
                        tmp_sta.push((bool_r || bool_l) ? "true1" : "false");
                    else
                        tmp_sta.push((bool_r && bool_l) ? "true1" : "false");
                }
            }
            return tmp_sta.top() == "true1";
        }
    }
