#include "student.h"

Student::Student() {

}

Student::Student(QString id, QString name, double gpa, int semester, int credit, int attempts, int program, int stream)
{
    Student::id = id;
    Student::name = name;
    Student::gpa = gpa;
    Student::semester = semester;
    Student::credit = credit;
    Student::attempts = attempts;
    Student::program = program;
    Student::stream = stream;

    studied_courses = new QVector<QVector<QPair<QString,int>>*>();
    for (int i=0; i<semester; i++) {
        QVector<QPair<QString,int>> *tmp = new QVector<QPair<QString,int>>();
        studied_courses->push_back(tmp);
    }
}

QString Student::getId() {
    return id;
}

QString Student::getName() {
    return name;
}

double Student::getGPA() {
    return gpa/attempts;
}

int Student::getSemester() {
    return semester;
}

int Student::getStream() {
    return stream;
}

int Student::getCouPerSem() {
    return cou_per_sem;
}

void Student::setCouPerSem(int cou) {
    cou_per_sem = cou;
}

QVector<QVector<QPair<QString,int>>*>* Student::getStudied_courses() {
    return studied_courses;
}

int Student::getProgram() {
    return program;
}

int Student::getCredit() {
    return credit;
}

void Student::setStudied_courses(QVector<QVector<QPair<QString,int>>*>* studied) {
    studied_courses = studied;
    for (int i=0; i<semester; i++) {
        QVector<QPair<QString,int>> *tmp = new QVector<QPair<QString,int>>();
        studied_courses->push_back(tmp);
    }
}
