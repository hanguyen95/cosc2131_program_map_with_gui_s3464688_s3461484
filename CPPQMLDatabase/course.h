#ifndef COURSE_H
#define COURSE_H

#include <QObject>
#include <QDebug>
#include <QStack>
#include <QVector>
#include <QStringList>
#include "session.h"
#include <QtCore/qmath.h>

class Course : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString c_code READ getCode NOTIFY codeChanged)
    Q_PROPERTY(QString c_desc READ getDesc NOTIFY descChanged)
    Q_PROPERTY(QString c_title READ getTitle NOTIFY titleChanged)
    Q_PROPERTY(int c_credit READ getCredit NOTIFY creditChanged)
    Q_PROPERTY(int c_avai READ getAvai NOTIFY avaiChanged)
    Q_PROPERTY(int c_year READ getYear NOTIFY yearChanged)
    Q_PROPERTY(QString c_prereq READ getConvertedPrereq NOTIFY prerequisiteChanged)
    Q_PROPERTY(QString c_stream READ getCStream NOTIFY streamChanged)
    Q_PROPERTY(int c_rating READ getRating NOTIFY ratingChanged)
    Q_PROPERTY(QString c_program READ getCProgram NOTIFY programChanged)
    Q_PROPERTY(int c_priority READ getPriority NOTIFY priorityChanged)
    Q_PROPERTY(QString c_status READ getStatus NOTIFY statusChanged)
    Q_PROPERTY(QString c_grade READ getGrade NOTIFY gradeChanged)
    Q_PROPERTY(bool c_study READ canStudy NOTIFY canStudyChanged)
    Q_PROPERTY(double c_avggpa READ getAvggpa NOTIFY avgChanged)
    Q_PROPERTY(double c_failrate READ getFailrate NOTIFY failrateChanged)

public:
    Course(QString code, QString desc, QString title, int credit, int avai, int year, QString prerequisite, int stream, int rating, int program, Session* ses);
    Course();
    QString getCode() const;
    QString getGrade() const;
    QString getDesc() const;
    QString getStatus();
    QString getTitle() const;
    int getCredit() const;
    int getAvai() const;
    int getYear() const;
    QString getPrereq() const;
    int getStream() const;
    int getRating() const;
    int getProgram() const;
    QString getConvertedPrereq();
    QString getCStream();
    QString getCProgram();
    int getPriority() const;
    void setCode(QString code);
    void setDesc(QString desc);
    void setTitle(QString title);
    void setCredit(int credit);
    void setAvai(int avai);
    void setYear(int year);
    void setPrereq(QString prerequisite);
    void setStream(int stream);
    void setRating(int rating);
    void setProgram(int program);
    void calcPriority();
    void setStudied(bool is_studied);
    void setPlanned(bool is_planned);
    bool isStudied();
    bool isPlanned();
    bool canStudy();
    double getAvggpa();
    double getFailrate();
    void setFailrate(double rate);
    void setAvggpa(double gpa);

signals:
    void codeChanged();
    void descChanged();
    void titleChanged();
    void creditChanged();
    void avaiChanged();
    void yearChanged();
    void prerequisiteChanged();
    void streamChanged();
    void ratingChanged();
    void programChanged();
    void priorityChanged();
    void statusChanged();
    void gradeChanged();
    void canStudyChanged();
    void avgChanged();
    void failrateChanged();

private:
    QString code;
    QString desc;
    QString title;
    double failrate = 0;
    double avggpa = 0;
    int credit;
    int avai;
    int year;
    QString prereq;
    int stream;
    int rating;
    int program;
    int priority;
    bool is_studied;
    bool is_planned;
    Session* ses;
};

#endif // COURSE_H
