#ifndef SERVICES_H
#define SERVICES_H

#include <QObject>
#include "dbconnect.h"
#include "session.h"
#include <QStringList>
#include <QQmlContext>
#include <QStringRef>
#include "course.h"
#include "programgraph.h"
#include <QStringList>
class QString;
extern QVector<Course *> courses;
extern ProgramGraph* pgraph;
class Services : public QObject
{
    Q_OBJECT
public:
    Services(QQmlContext *ctxt, Session *sess);
    Q_INVOKABLE bool doLogin(QString username, QString password);
    Q_INVOKABLE void getCourseData();
    Q_INVOKABLE void getAllCourses();
    Q_INVOKABLE QString getUserRole();
    Q_INVOKABLE void getUserInformation();
    Q_INVOKABLE void retrieveStudentInformation();
    Q_INVOKABLE void getCurSem();
    Q_INVOKABLE void getStreamProgramData();
    Q_INVOKABLE void planning();
    Q_INVOKABLE void find_course(QString code);
    Q_INVOKABLE void resetData();
    Q_INVOKABLE void enroll(QString code);
    Q_INVOKABLE void find_student(QString sid);
    QVector<QString> tokenize_prereq(QString prereq);
    bool compare_precedence(QString token1, QString token2);
    QString convertPrereq(QString prereq);
    Q_INVOKABLE bool create_course(QString co, QString ti, QString pr, QString st, QString ye,QString de, QString pre,int avai,QString ra, QString cre);
    Q_INVOKABLE QStringList getProgramList();
    Q_INVOKABLE QStringList getStreamList(QString program);

public slots:
    void changeName(QString username);
private:
    DBConnect *m_con;
    QQmlContext *m_ctxt;
    Session *m_ses;
};

#endif // SERVICES_H
