#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>
#include <QVector>
#include <QPair>

class Student : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString s_id READ getId)
    Q_PROPERTY(QString s_name READ getName)
    Q_PROPERTY(int s_credit READ getCredit)
    Q_PROPERTY(int s_stream READ getStream)
    Q_PROPERTY(int s_semester READ getSemester)
    Q_PROPERTY(int s_program READ getProgram)
    Q_PROPERTY(int s_cou_per_sem READ getCouPerSem WRITE setCouPerSem NOTIFY priorityChanged)
    Q_PROPERTY(double s_gpa READ getGPA)

public:
    Student();
    Student(QString id, QString name, double gpa, int semester, int credit, int attempts, int program, int stream);
    QString getId();
    QString getName();
    int getCredit();
    int getSemester();
    int getCouPerSem();
    void setCouPerSem(int cou);
    double getGPA();
    int getProgram();
    int getStream();
    QVector<QVector<QPair<QString, int> > *>* getStudied_courses();
    void setStudied_courses(QVector<QVector<QPair<QString,int>>*>* studied);

signals:
    void priorityChanged();

private:
    QString id;
    QString name;
    double gpa;
    int semester;
    int credit;
    int attempts;
    int program;
    int stream;
    int cou_per_sem = 3;
    QVector<QVector<QPair<QString, int>> *> *studied_courses;
};

#endif // STUDENT_H
