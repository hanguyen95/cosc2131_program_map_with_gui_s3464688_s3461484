import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
        signal doSearch(string searchText)
        signal searchTextChanged(string searchText)

        function showSearch() {
            pageLoader.source = "CourseResult1.qml"
        }
        function showSearch1() {
            pageLoader1.source = "StudentResult.qml"
        }

        RowLayout {
            id: searchBarStudent
            anchors.margins: 30
            width: 350
            height: 50
            Behavior on opacity { NumberAnimation{} }
            visible: opacity ? true : false
            TextField {
                id: searchTextStudent
                font{
                    pixelSize: 14
                    bold: false
                    family: "Champagne & Limousines"
                }
                Behavior on opacity { NumberAnimation{} }
                visible: opacity ? true : false
                property bool ignoreTextChange: false
                placeholderText: qsTr("Type Student ID...")
                Layout.fillWidth: true
                onTextChanged: {
                    if (!ignoreTextChange)
                        searchTextChanged(text)
                }
                onAccepted: {
                    Services.find_student(searchTextStudent.text);
                    showSearch1();
                }
            }
        }

        ToolButton {
            id: searchButtonStudent
            anchors{
                left: searchBarStudent.right
                leftMargin: 10
                top: searchBarStudent.top
                topMargin: 10
            }
            Image {
                source:  "../CPPQMLDatabase/pictures/search.png"
                width: 28
                height: 28
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                doSearch(searchTextStudent.text);
                Services.find_student(searchTextStudent.text);
                showSearch1();
            }
        }

        Loader {
            id:pageLoader1
            width: parent.width4
            anchors.top:searchBarStudent.top
            height: 60
        }

        RowLayout {
            id: searchBarCourse
            anchors.margins: 30
            width: 350
            height: 50
            anchors.top: pageLoader1.bottom
            Behavior on opacity { NumberAnimation{} }
            visible: opacity ? true : false
            TextField {
                id: searchTextCourse
                font{
                    pixelSize: 14
                    bold: false
                    family: "Champagne & Limousines"
                }
                Behavior on opacity { NumberAnimation{} }
                visible: opacity ? true : false
                property bool ignoreTextChange: false
                placeholderText: qsTr("Type course code...")
                Layout.fillWidth: true
                onTextChanged: {
                    if (!ignoreTextChange)
                        searchTextChanged(text)
                }
                onAccepted: {
                    Services.find_course(searchTextCourse.text);
                    showSearch();
                }
            }
        }

        ToolButton {
            id: searchButtonCourse
            anchors{
                left: searchBarCourse.right
                leftMargin: 10
                top: searchBarCourse.top
                topMargin: 10
            }
            Image {
                source:  "../CPPQMLDatabase/pictures/search.png"
                width: 28
                height: 28
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                doSearch(searchTextCourse.text);
                Services.find_course(searchTextCourse.text);
                showSearch();
            }
        }
        Loader {
            id:pageLoader
            width: parent.width
            height: 120
            anchors.top: searchBarCourse.top
        }
}

