import QtQuick 2.0

ListView{
    id: root
    property bool isDeletable: false
    signal selected(var c_code, var c_title, var c_credit, var c_status, var c_grade)
    signal deleted(var c_code)
    clip: true
    model: Course
    boundsBehavior: Flickable.StopAtBounds    
    onSelected: {
        doShow(c_code);
    }
    header:Item{
        width: parent.width
        height: 35
        anchors.topMargin: 10
        Component{
            id: rectheader
            Rectangle{
                width: headerWidth
                height: 35
                color: "grey"
                Text{
                    id: nameText
                    color: "white"
                    text: qsTr(headername)
                    anchors.centerIn: parent
                    anchors.margins: 4
                    font{
                        pixelSize: 15
                        family: "Champagne & Limousines"
                        bold: true
                    }
                }
            }
        }
        Loader{
            id: headerCCode;
            property int headerWidth: parent.width*0.2
            property string headername: "Course Code";
            sourceComponent: rectheader;
            anchors{left: parent.left}
        }
        Loader{
            id: headerCTitle;
            property int headerWidth: parent.width*0.4
            property string headername: "Course Title";
            sourceComponent: rectheader;
            anchors{left: headerCCode.right}
        }
        Loader{
            id: headerCredit;
            property int headerWidth: parent.width*0.1
            property string headername: "Credits";
            sourceComponent: rectheader;
            anchors{left: headerCTitle.right}
        }
        Loader{
            id: headerStatus;
            property int headerWidth: parent.width*0.2
            property string headername: "Status";
            sourceComponent: rectheader;
            anchors{left: headerCredit.right}
        }
        Loader{
            id: headerGrade;
            property int headerWidth: parent.width*0.1
            property string headername: "Grade";
            sourceComponent: rectheader;
            anchors{left: headerStatus.right}
        }
    }
    delegate:Item{
        id: del
        width: parent.width
        height: 25
        MouseArea{
            anchors.fill: parent
            onClicked: {
                root.selected(c_code,c_title,c_credit, c_status);
            }
        }

        Component{
            id: rectview
            Rectangle{
//                Text{
//                    text: "X"
//                    font.bold: true
//                    color: "red"
//                    anchors{
//                        left: parent.left
//                        leftMargin: 5
//                        verticalCenter: parent.verticalCenter
//                    }
//                    visible: isShow
//                    MouseArea{
//                        anchors.fill: parent
//                        onClicked: {
//                            root.deleted(c_code);
//                        }
//                    }
//                }
                width:loadWidth
                height: del.height
                color: c_status === "Completed" ? "#8a001e" : "lightgrey"
                border.width: 1
                border.color: c_status === "Completed" ? "black" : "grey"
                Text{
                    id: nameText
                    color: c_status === "Completed" ? "white" : "grey"
                    text: name
                    anchors.centerIn: parent
                    anchors.margins: 5
                    wrapMode: Text.Wrap
                    font{
                        pixelSize: 13
                        family: "Fira Sans"

                    }
                }
            }
        }
        Loader{
            id: loadCCode;
            property int loadWidth: del.width*0.2
            property string name: c_code;
            //property bool isShow: isDeletable;
            sourceComponent: rectview;
            anchors{left: parent.left}
        }
        Loader{
            id: loadCTitle;
            property int loadWidth: del.width*0.4
            property string name: c_title;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{left: loadCCode.right}
        }
        Loader{
            id: loadCredit;
            property int loadWidth: del.width*0.1
            property string name: c_credit;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{left: loadCTitle.right}
        }
        Loader{
            id: loadStatus;
            property int loadWidth: del.width*0.2
            property string name: c_status;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{left: loadCredit.right}
        }
        Loader{
            id: loadGrade;
            property int loadWidth: del.width*0.1
            property string name: c_grade;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{left: loadStatus.right}
        }
    }
}
