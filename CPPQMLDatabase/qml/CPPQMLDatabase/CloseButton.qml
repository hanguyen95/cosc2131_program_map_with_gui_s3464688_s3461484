import QtQuick 2.0

    Rectangle{
        width: 15
        height: 15
        anchors{
            right: parent.right
            rightMargin: 5
            top: parent.top
            topMargin: 5
        }
        border.color: "#ac1c1c"
        color: "#ac1c1c"
        opacity: 1
        //        radius: 3
        smooth: true
        Text{
            text: "x"
            anchors.verticalCenterOffset: -3
            anchors.horizontalCenterOffset: 0
            style: Text.Normal
            font.italic: false
            font.pointSize: 23
            textFormat: Text.AutoText
            font.family: "Verdana"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            color: "white"
            font{bold: false}
            anchors.centerIn: parent
        }
        MouseArea{
            anchors.fill: parent
            onClicked: Qt.quit();
        }
    }



