import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    function doInsert(){
        if(couCodeBox.text.trim()==="" || couTitleBox.text.trim()===""
                || couPrereqBox.text.trim() === "" || couCreditBox.text.trim()===""
                || couDescBox.text.trim()==="" || couStreamBox.currentText === "Choose Stream"
                || couProgramBox.currentText==="Choose Program"||couYearBox.text.trim() ==="")
        {
            errMsg.text = "All fields must be filled";
            errMsg.color = "red";
            errMsg.visible = "true";
        }
        else{
            var tmp = 0;
            if (semA.checked) tmp+=1;
            if (semB.checked) tmp+=2;
            if (semC.checked) tmp+=4;
            var result = Services.create_course(couCodeBox.text.trim(),couTitleBox.text.trim(),couProgramBox.currentText,couStreamBox.currentText,couYearBox.text.trim(),couDescBox.text.trim(),couPrereqBox.text.trim(),tmp,couRateBox.currentText,couCreditBox.text.trim());
            if(result)
            {
                errMsg.text = "Course created succesfully";
                errMsg.color = "green";
                errMsg.visible = "true";
            }
            else
            {
                errMsg.text = "Failed to create course";
                errMsg.color = "red";
                errMsg.visible = "true";
            }
        }
    }
    Text{
        id: couCode
        text: "Course Code" // title, Prerequisite, credit points, year, available, Description, stream, program id
        anchors{
            left: parent.left
            leftMargin: 10
            top: parent.top
            topMargin: 10
            rightMargin: 30
        }
    }
    TextBox{
        id: couCodeBox
        width: 200
        height: 20
        anchors{
            leftMargin: 20
            left: couCode.right
            verticalCenter: couCode.verticalCenter
        }
        isOnlyDigitAndAlphabet: true
        isPassword: false
        maxLength: 8
    }
    Text{
        id: couTitle
        text: "Course Title"
        anchors{
            left: couCode.left
            top: couCode.bottom
            topMargin: 10
        }
    }
    TextBox{
        id: couTitleBox
        width: 200
        height: 20
        anchors{
            left: couCodeBox.left
            verticalCenter: couTitle.verticalCenter
        }
        isOnlyDigitAndAlphabet: false
        isPassword: false
    }
    Text{
        id: couPrereq
        text: "Prerequisite"
        anchors{
            left: couTitle.left
            top: couTitle.bottom
            topMargin: 10
        }
    }
    TextBox{
        id: couPrereqBox
        width: 200
        height: 20
        anchors{
            left: couCodeBox.left
            verticalCenter: couPrereq.verticalCenter
        }
        isOnlyDigitAndAlphabet: false
        isPassword: false
    }
    Text{
        id: couRate
        text: "Rating"
        anchors{
            left: couPrereqBox.right
            verticalCenter: couPrereq.verticalCenter
            topMargin: 10
            leftMargin: 20
        }
    }
    ComboBox{
        id: couRateBox
        width: 50
        height: 20
        model: ["1","2","3"]
        anchors{
            left: couRate.right
            leftMargin: 20
            verticalCenter: couRate.verticalCenter
        }
    }
    Text{
        id: couCredit
        text: "Credits"
        anchors{
            left: couPrereq.left
            top: couPrereq.bottom
            topMargin: 10
        }
    }
    TextBox{
        id: couCreditBox
        width: 50
        height: 20
        anchors{
            left: couCodeBox.left
            verticalCenter: couCredit.verticalCenter
        }
        isOnlyDigit: true
        isPassword: false
    }
    Text{
        id: couYear
        text: "Year"
        anchors{
            left: couCode.left
            top: couCredit.bottom
            topMargin: 10
        }
    }
    TextBox{
        id: couYearBox
        width: 200
        height: 20
        anchors{
            left: couCodeBox.left
            verticalCenter: couYear.verticalCenter
        }
        isOnlyDigit: true
        isPassword: false
    }
    Text{
        id: couAvai
        text: "Available"
        anchors{
            left: couYear.left
            top: couYear.bottom
            topMargin: 10
        }
    }
    CheckBox {
        id: semA
        text: "Sem A"
        checked: false
        anchors {
            left: couCodeBox.left
            verticalCenter: couAvai.verticalCenter
        }
    }
    CheckBox {
        id: semB
        text: "Sem B"
        checked: false
        anchors {
            left: semA.right
            leftMargin: 20
            verticalCenter: couAvai.verticalCenter
        }
    }
    CheckBox {
        id: semC
        text: "Sem C"
        checked: false
        anchors {
            left: semB.right
            leftMargin: 20
            verticalCenter: couAvai.verticalCenter
        }
    }
    Text{
        id: couDesc
        text: "Description"
        anchors{
            left: couAvai.left
            top: couAvai.bottom
            topMargin: 10
        }
    }
    TextBox{
        id: couDescBox
        width: 200
        height: 20
        anchors{
            left: couCodeBox.left
            verticalCenter: couDesc.verticalCenter
        }
        isOnlyDigitAndAlphabet: false
        isPassword: false
    }
    Text{
        id: couProgram
        text: "Program"
        anchors{
            left: couDesc.left
            top: couDesc.bottom
            topMargin: 10
        }
    }
    ComboBox{
        id: couProgramBox
        width: 200
        height: 20
        model: Services.getProgramList()
        anchors{
            left: couCodeBox.left
            verticalCenter: couProgram.verticalCenter
        }
        activeFocusOnPress: true
    }
    Text{
        id: couStream
        text: "Stream"
        anchors{
            left: couProgram.left
            top: couProgram.bottom
            topMargin: 10
        }
    }
    ComboBox{
        id: couStreamBox
        width: 200
        height: 20
        model: Services.getStreamList(couProgramBox.currentText)
        anchors{
            left: couCodeBox.left
            verticalCenter: couStream.verticalCenter
        }
        activeFocusOnPress: true
    }
//    Text{
//        id: txtClass
//        text: "Class"
//        anchors{
//            left: txtCourseOutline.left
//            top: txtCourseOutline.bottom
//            topMargin: 10
//        }
//    }
//    TextBox{
//        id: txtBoxClass
//        width: 50
//        height: 20
//        anchors{
//            left: cmbAst.left
//            verticalCenter: txtClass.verticalCenter
//        }
//        isOnlyDigitAndAlphabet: true
//        isPassword: false
//        maxLength: 5
//    }
//    Text{
//        id: txtRoom
//        text: "Room"
//        anchors{
//            left: txtClass.left
//            top: txtClass.bottom
//            topMargin: 10
//        }
//    }
//    TextBox{
//        id: txtBoxRoom
//        width: 50
//        height: 20
//        isOnlyDigitAndAlphabet: true
//        anchors{
//            left: cmbAst.left
//            verticalCenter: txtRoom.verticalCenter
//        }
//        isPassword: false
//        maxLength: 5
//    }
//    Text{
//        id: txtMeeting
//        text: "Meeting"
//        anchors{
//            left: txtRoom.left
//            top: txtRoom.bottom
//            topMargin: 10
//        }
//    }
//    TextBox{
//        id: txtBoxMeeting
//        isOnlyDigit: true
//        width: 30
//        height: 20
//        anchors{
//            left: cmbAst.left
//            verticalCenter: txtMeeting.verticalCenter
//        }
//        isPassword: false
//        maxLength: 2
//    }
//    Text{
//        id: txtStartTime
//        text: "StartTime"
//        anchors{
//            left: txtMeeting.left
//            top: txtMeeting.bottom
//            topMargin: 10
//        }
//    }
//    TextBox{
//        id: txtBoxStartTime
//        isDate: true
//        width: 150
//        height: 20
//        anchors{
//            left: cmbAst.left
//            verticalCenter: txtStartTime.verticalCenter
//        }
//        maxLength: 16
//    }
//    Text{
//        id: txtEndTime
//        text: "EndTime"
//        anchors{
//            left: txtStartTime.left
//            top: txtStartTime.bottom
//            topMargin: 10
//        }
//    }
//    TextBox{
//        id: txtBoxEndTime
//        isDate: true
//        width: 150
//        height: 20
//        anchors{
//            left: cmbAst.left
//            verticalCenter: txtEndTime.verticalCenter
//        }
//        maxLength: 16
//    }

    Rectangle{
        id: btnsave
//        radius: 3
        width: 50
        height: 20
        Text{
            text: "Save"
            anchors.centerIn: parent
        }
        anchors{
            top: couStreamBox.bottom
            topMargin: 10
            right: couStreamBox.right
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                doInsert();
            }
        }
    }
    Text{
        id: errMsg
        visible:false
        anchors{
            verticalCenter: btnsave.verticalCenter
            right: btnsave.left
            rightMargin: 10
        }
    }

}
