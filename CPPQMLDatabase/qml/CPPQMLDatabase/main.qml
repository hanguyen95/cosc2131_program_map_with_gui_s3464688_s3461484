import QtQuick 2.0
import QtQuick.Window 2.0

Rectangle{
    id: container

    function changeWindowSize(mainwindow,width,height){
        mainwindow.setWidth(width);
        mainwindow.setHeight(height);
        mainwindow.setX((Screen.width-width)/2);
        mainwindow.setY((Screen.height-height)/2);
    }

    Loader{
        id: loader
        anchors{
            centerIn: parent
        }
    }
    states: [
        State{
            name: "Login"
            PropertyChanges {
                target: loader
                source: "Login.qml"
            }
            StateChangeScript{
                name: "changeMainWindow"
                script: changeWindowSize(Mainwindow,300,150);
            }
        },
        State{
            name: "Student"
            PropertyChanges {
                target: loader
                source: "Student.qml"
            }
            StateChangeScript{
                name: "changeMainWindow"
                script: changeWindowSize(Mainwindow,780,535);
            }
        },
        State{
            name: "Manager"
            PropertyChanges {
                target: loader
                source: "Manager.qml"
            }
            StateChangeScript{
                name: "changeMainWindow"
                script: changeWindowSize(Mainwindow,780,535);
            }
        }

    ]
    Component.onCompleted: container.state = "Login"
}
