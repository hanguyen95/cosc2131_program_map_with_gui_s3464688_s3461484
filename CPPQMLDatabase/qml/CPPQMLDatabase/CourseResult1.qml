import QtQuick 2.0
ListView {
    anchors.top: parent.bottom
    orientation: ListView.VerticalTopToBottom
    id: root
//    property bool isDeletable: false
    model: Result


    delegate:Item {
        id: del
        width: parent.width
        height: parent.height

        Component{
            id: rectview
            Rectangle{
                width:  loadWidth
                height: 20
                Text{
                    anchors {
                        left: parent.left
                        leftMargin: 35
                        top: parent.top
                    }

                    id: nameText
                    color: "#8a001e"
                    opacity: 0.9
                    text: name
                    wrapMode: Text.WordWrap
                    font{
                        pixelSize: 19
                        family: "Red World"
                        bold: false
                    }
                }
            }
        }
        Loader{
            id: loadCCodeL;
            property int loadWidth: del.width*0.2
            property string name: c_code === "" ? "Not found." : c_code;
            //property bool isShow: isDeletable;
            sourceComponent: rectview;
            anchors{left: parent.left}
        }
        Loader{
            id: loadCTitleL;
            property int loadWidth: del.width*0.4
            property string name: "Course Name: "+ c_title;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{
                left: loadCCodeL.right}
            }

        Loader{
            id: loadCDescL;
            property int loadWidth: del.width
            property string name: "Description: " + c_desc;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{
                top: loadCCodeL.bottom
                //left: parent.left
            }
        }
        Loader{
            id: loadCPrereqL;
            property int loadWidth: del.width*0.7
            property string name: "Prerequisites: "+c_prereq;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{
                top: loadCDescL.bottom
                //left: parent.left
            }
        }

        Loader{
            id: enrollButton
            visible: true
            anchors{
                left: parent.left
                leftMargin: 450
                topMargin: 10
            }
            Image {
                source:  "../CPPQMLDatabase/pictures/enroll.png"
                width: 48
                height: 48
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        Services.enroll(c_code);
                    }
                }
            }
        }
    }
    }

