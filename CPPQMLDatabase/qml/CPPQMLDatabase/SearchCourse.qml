import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {

        signal doSearch(string searchText)
        signal searchTextChanged(string searchText)

        function showSearch() {
            pageLoader.source = "CourseResult.qml"
        }

        RowLayout {
            id: searchBar
            anchors.margins: 30
            width: 350
            height: 50
            Behavior on opacity { NumberAnimation{} }
            visible: opacity ? true : false
            TextField {
                id: searchText
                font{
                    pixelSize: 14
                    bold: false
                    family: "Bebas Neue"
                }
                Behavior on opacity { NumberAnimation{} }
                visible: opacity ? true : false
                property bool ignoreTextChange: false
                placeholderText: qsTr("Type course code...")
                Layout.fillWidth: true
                onTextChanged: {
                    if (!ignoreTextChange)
                        searchTextChanged(text)
                }
                onAccepted: {
                    Services.find_course(searchText.text);
                    showSearch();
                }
            }
        }

        ToolButton {
            id: searchButton
            anchors{
                left: searchBar.right
                leftMargin: 10
                top: searchBar.top
                topMargin: 10
            }
            Image {
                source:  "../CPPQMLDatabase/pictures/search.png"
                width: 28
                height: 28
                fillMode: Image.PreserveAspectFit
            }
            onClicked: {
                doSearch(searchText.text);
                Services.find_course(searchText.text);
                showSearch();
            }
        }

        Loader {
            id:pageLoader
            width: parent.width
            height: 120
            anchors.top: searchBar.top
        }

}

