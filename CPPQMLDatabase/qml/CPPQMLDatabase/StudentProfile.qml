import QtQuick 2.0

Item {
    height: 40
    Text{
        id: sid
        width: 117
        height: 17
        anchors{
            left: parent.left
            leftMargin: 248
            top: parent.top
        }
        color: "#8a001e"
        text: "Student ID:" + Session.username
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
    Text{
        id: name
        width: 261
        height: 17
        text: "Student Name: " + Session.name
        anchors{
            left: parent.left
            leftMargin: 10
            top: parent.top
        }
        color: "#8a001e"
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
    Text{
        id: gpa
        text: "GPA: " + Session.gpa
        anchors{
            left: parent.left
            leftMargin: 501
            top: parent.top
            topMargin: 2
        }
        color: "#8a001e"
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
    Text{
        id: stream
        width: 178
        height: 17
        text: "Stream/Minor: " + Session.stream
        anchors{
            left: parent.left
            leftMargin: 248
            top: parent.top
            topMargin: 23
        }
        color: "#8a001e"
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
    Text{
        id: program
        width: 261
        height: 17
        text: "Program: " + Session.program
        anchors{
            left: parent.left
            leftMargin: 10
            top: parent.top
            topMargin: 23
        }
        color: "#8a001e"
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
    Text{
        id: credit
        width: 263
        height: 17
        text: "Completed Credit: " + Session.credit
        anchors{
            left: parent.left
            leftMargin: 501
            top: parent.top
            topMargin: 23
        }
        color: "#8a001e"
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
}

