import QtQuick 2.0
    ListView{
        id: root
        property bool isDeletable: false
        signal selected(var c_code)
        clip: true
        snapMode: ListView.SnapToItem
        orientation: ListView.Vertical
        model: AllCourses
        boundsBehavior: Flickable.StopAtBounds
        header:Item{
            width: parent.width
            height: 35
            anchors.topMargin: 10
            Component{
                id: rectheader
                Rectangle{
                    width: headerWidth
                    height: 35
                    color: "grey"
                    Text{
                        id: nameText
                        color: "white"
                        text: qsTr(headername)
                        anchors.centerIn: parent
                        anchors.margins: 4
                        font{
                            pixelSize: 15
                            family: "Champagne & Limousines"
                            bold: true
                        }
                    }
                }
            }
            Loader{
                id: headerCCodeL;
                property int headerWidth: parent.width*0.1
                property string headername: "Code";
                sourceComponent: rectheader;
                anchors{left: parent.left}
            }
            Loader{
                id: headerCTitleL;
                property int headerWidth: parent.width*0.4
                property string headername: "Course Title";
                sourceComponent: rectheader;
                anchors{left: headerCCodeL.right}
            }
            Loader{
                id: headerCreditL;
                property int headerWidth: parent.width*0.1
                property string headername: "Credits";
                sourceComponent: rectheader;
                anchors{left: headerCTitleL.right}
            }
            Loader{
                id: headerStatusL;
                property int headerWidth: parent.width*0.1
                property string headername: "Status";
                sourceComponent: rectheader;
                anchors{left: headerCreditL.right}
            }
            Loader{
                id: headerFeb;
                property int headerWidth: parent.width*0.1
                property string headername: "Feb Sem";
                sourceComponent: rectheader;
                anchors{left: headerStatusL.right}
            }
            Loader{
                id: headerJun;
                property int headerWidth: parent.width*0.1
                property string headername: "Jun Sem";
                sourceComponent: rectheader;
                anchors{left: headerFeb.right}
            }
            Loader{
                id: headerOct;
                property int headerWidth: parent.width*0.1
                property string headername: "Oct Sem";
                sourceComponent: rectheader;
                anchors{left: headerJun.right}
            }
        }
        delegate:Item{
            id: del
            width: parent.width
            height: 25

            Loader {
                id:pageLoader
                width: parent.width
                height: 40
                anchors.top: parent.bottom
                anchors.bottomMargin: 45
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onExited: {
                    enroll.visible = false;
                    prereq.visible = false;
                }
                onHoveredChanged: {
                    enroll.visible = true;
                    prereq.visible = true;
                }

            }

            Component{
                id: rectview            

                Rectangle{
                    width:loadWidth
                    height: del.height
                    color: "#DBCFCF"
                    border.width: 1
                    border.color: "white"
                    Text{
                        id: nameText
                        color: "#8a001e"
                        text: name
                        anchors.centerIn: parent
                        anchors.margins: 5
                        wrapMode: Text.Wrap
                        font{
                            pixelSize: 12
                            family: "Fira Sans"

                        }

                    }

                }

            }
            Loader{
                id: loadCCodeL;
                property int loadWidth: del.width*0.1
                property string name: c_code;
                //property bool isShow: isDeletable;
                sourceComponent: rectview;
                anchors{left: parent.left}
            }

            Loader{
                id: loadCTitleL;
                property int loadWidth: del.width*0.4
                property string name: c_title;
                //property bool isShow: false;
                sourceComponent: rectview;
                anchors{left: loadCCodeL.right}
            }
            Loader{
                id: loadCreditL;
                property int loadWidth: del.width*0.1
                property string name: c_credit;
                //property bool isShow: false;
                sourceComponent: rectview;
                anchors{left: loadCTitleL.right}
            }
            Loader{
                id: loadStatusL;
                property int loadWidth: del.width*0.1
                property string name: c_status;
                //property bool isShow: false;
                sourceComponent: rectview;
                anchors{left: loadCreditL.right}
            }
            Loader{
                id: loadFeb;
                property int loadWidth: del.width*0.1
                property string name: (c_avai & 1) === 1 ? "x" : ""
                //property bool isShow: false;
                sourceComponent: rectview;
                anchors{left: loadStatusL.right}
            }
            Loader{
                id: loadJun;
                property int loadWidth: del.width*0.1
                property string name: (c_avai & 2) === 2 ? "x" : ""
                //property bool isShow: false;
                sourceComponent: rectview;
                anchors{left: loadFeb.right}
            }
            Loader{
                id: loadOct;
                property int loadWidth: del.width*0.1
                property string name: (c_avai & 4) === 4 ? "x" : ""
                //property bool isShow: false;
                sourceComponent: rectview;
                anchors{left: loadJun.right}
            }

            Loader{
                id: enroll;
                visible: false;
                property int loadWidth: del.width*0.3
                property string name: c_study ? "Enrollable" : "Not Enrollable"
                //property bool isShow: false;
                sourceComponent: rectview;
                //anchors{left: parent.left}
            }
            Loader{
                id: prereq;
                visible: false;
                property int loadWidth: del.width*0.7
                property string name: "Prerequisites: " + c_prereq
                //property bool isShow: false;
                sourceComponent: rectview;
                anchors{left: enroll.right}
            }
        }

}
