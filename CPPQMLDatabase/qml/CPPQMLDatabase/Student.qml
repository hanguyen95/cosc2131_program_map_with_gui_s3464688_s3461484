import QtQuick 2.0

Rectangle {
    width: 780
    height: 535
    border.width: 3
    border.color: "grey"


    Logo{
        id: logo
        anchors{
            left: parent.left
            leftMargin: 10
            top: parent.top
            topMargin: 30
        }
    }

    Text{
        text: "Student's Enrollment"
        anchors.verticalCenterOffset: -12
        textFormat: Text.RichText
        color: "#8a001e"
        font{
            pixelSize: 34
            bold: false
            family: "Bebas Neue"
        }
        anchors{
            verticalCenter: logo.verticalCenter
            left: logo.right
            leftMargin: 10
        }
        style: Text.Normal
    }

    Text{
        id: welcome
        y: 86
        text: "Welcome, "+Session.name +" | "
        anchors.bottomMargin: 4
        color: "#8a001e"
        font{
            pixelSize: 15
            bold: true
            family: "Champagne & Limousines"
        }
        anchors{
            bottom: logo.bottom
            left: logo.right
            leftMargin: 10
        }
        //style: Text.Raised
    }

    Text{
        id:signout
        y: 86
        text: "Sign Out"
        anchors.bottomMargin: 4
        anchors.leftMargin: 0
        color: "#8a001e"
        font{
            pixelSize: 15
            bold: true
            family: "Champagne & Limousines"
        }
        anchors{
            bottom: logo.bottom
            left: welcome.right
        }
        //style: Text.Raised
        MouseArea{
            anchors.fill: parent
            onClicked: {
                Services.resetData();
                container.state = "Login"
            }
            hoverEnabled: true
            onEntered: {
                signout.color = "gold";
            }
            onExited: {
                signout.color = "gray";
            }
        }
    }

    StudentProfile {
        id: stupro
        anchors{
            top: logo.bottom
            topMargin: 20
            leftMargin: 50
            left: parent.left
            right: parent.right
        }
    }

    StudentTabBar{
        id: stutab
        anchors{
            top: stupro.bottom
            topMargin: 15
            bottom: parent.bottom
            bottomMargin: 10
            left: parent.left
            leftMargin: 10
            right: parent.right
            rightMargin: 10
        }
    }

    DragBox{}
    CloseButton{}
}
