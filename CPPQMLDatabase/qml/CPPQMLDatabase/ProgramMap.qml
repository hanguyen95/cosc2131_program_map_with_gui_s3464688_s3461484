import QtQuick 2.0

GridView{
    id: root
    clip: true
    snapMode: ListView.SnapToItem
    cellWidth: 230; cellHeight: 80
    model: Plan
    //anchors.left: parent.left
    anchors.leftMargin: 93
    //anchors.top: parent.top
    anchors.topMargin: 30
    boundsBehavior: Flickable.StopAtBounds
    delegate:Item{
        function make_color(c_prereq, c_status) {
            if(c_status === "Completed") {
                return "#688D3B";
            }
            else if(c_status === "In Progress") {
                return "#E9C865";
            }
            if(!c_prereq==="No Prerequisite.") {
                return "#FCFBF9";
            }
            else {
                return "#9D9B98";
            }
         }
        id: del
        width: parent.width
        height: 60
        Loader {
            id:pageLoader
            width: parent.width
            height: 40
            anchors.top: parent.bottom
            anchors.bottomMargin: 45
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onExited: {

            }
            onHoveredChanged: {

            }

        }

        Component{
            id: rectview

            Rectangle{
                width:loadWidth
                height: del.height-20
                anchors.margins: 20
                color: del.make_color(c_prereq, c_status)
                border.width: 1
                border.color: "white"
                Text{
                    id: nameText
                    color: "white"
                    text: name
                    anchors.centerIn: parent
                    anchors.margins: 5
                    wrapMode: Text.WordWrap
                    font{
                        pixelSize: 12
                        family: "Fira Sans"

                    }

                }

            }

        }
        Loader{
            id: loadCCodeL;
            property int loadWidth: del.width*0.1
            property string name: c_code;
            //property bool isShow: isDeletable;
            sourceComponent: rectview;
            anchors{left: parent.left}
        }
    }
}
