import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: login
    width: 300
    height: 150
    radius: 0
    opacity: 1
    border.width: 7

    Logo{
        id: logo
        anchors.verticalCenterOffset: 0
        anchors{
            left: parent.left
            leftMargin: 15
            verticalCenter: parent.verticalCenter
        }
    }

    Text{
        id: username
        text: "Username"
        anchors.topMargin: 0
        font.family: "Red World"
        color: "grey"
        font{
            pixelSize: 18
            bold: false
        }
        anchors{
            top : logo.top
            left : logo.right
            leftMargin: 19
        }
    }

    TextBox{
        id: textUsername
        width: 150
        height: 20
        radius: 0
        border.color: "#602d2d"
        opacity: 0.7
        border.width: 1
        maxLength: 20
        anchors{
            left: logo.right
            leftMargin: 19
            top: username.bottom
            topMargin: 1
        }
        isFocus: true
    }

    Text{
        id: password
        y: 74
        text: "Password"
        font.family: "Red World"
        color: "grey"
        font{
            pixelSize: 18
            bold: false
        }
        anchors{
            bottom : textPassword.top
            bottomMargin: 1
            left : logo.right
            leftMargin: 19
        }
    }

    TextBox{
        id: textPassword
        y: 95
        width: 150
        height: 20
        radius: 0
        anchors.bottomMargin: 0
        border.color: "#602d2d"
        opacity: 0.7
        maxLength: 19
        anchors{
            left: logo.right
            leftMargin: 19
            bottom: logo.bottom
        }
        isPassword: true
        onEnter: {
            if(textPassword.text.trim()==="" || textUsername.text.trim()==="")
            {
                err.visible = true;
                err.text = "Username and password must be filled"
            }
            // invoke function from Services.h
            else if(Services.doLogin(textUsername.text,textPassword.text))
            {
                Session.username = textUsername.text
                Services.getUserInformation();
                Services.getCurSem()
                Services.getStreamProgramData()
                Services.getCourseData()
                Services.getAllCourses()
                if(Services.getUserRole()==="Manager")
                {
                    container.state = "Manager";
                }
                else
                {
                    Services.planning();
                    container.state = "Student";
                }

            }
            else
            {
                err.visible = true;
                err.text = "Wrong Username or password"
            }
        }
    }

    Text {
        id: err
        color: "#602d2d"
        visible: false
        anchors{
            top: textPassword.bottom
            topMargin: 5
            horizontalCenter: parent.horizontalCenter
        }
        font.family: "Red World"
        font{
            pixelSize: 15
            bold: false
        }
    }
    DragBox{}

    CloseButton{}
    border.color: "#33808080"
    Keys.onTabPressed: {
        if(textUsername.isFocus)textPassword.isFocus = true;
        else textUsername.isFocus = true;
    }
}

