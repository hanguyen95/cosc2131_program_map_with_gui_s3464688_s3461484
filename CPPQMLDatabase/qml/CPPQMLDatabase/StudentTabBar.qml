import QtQuick 2.0

Item {
    Rectangle{
        id: tab1
        width: 120
        height: 45
        color: "#8a001e"
        Text{
            text: "Student's Enroll"
            color: "white"
            font{
                pixelSize: 14
                bold: false
                family: "Bebas Neue"
            }

            anchors{
                top: parent.top
                topMargin: 5
                //bottomMargin: 5
                horizontalCenter: parent.horizontalCenter
            }
        }
        anchors{
            top: parent.top
            left: parent.left
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                parent.color = "#8a001e";
                tab2.color = "lightgrey";
                tab3.color = "lightgrey";
                tab4.color = "lightgrey";
                loadcom.source = "StudentEnroll.qml";
            }
        }
    }
    Rectangle{
        id: tab2
        width: 120
        height: 45
        color: "lightgrey"
        Text{
            text: "Program Map"
            color: "white"
            font{
                pixelSize: 14
                bold: false
                family: "Bebas Neue"
            }
            anchors{
                top: parent.top
                topMargin: 5
                //bottomMargin: 5
                horizontalCenter: parent.horizontalCenter
            }
        }
        anchors{
            top: parent.top
            left: tab1.right
            leftMargin: 5
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                parent.color = "#8a001e";
                tab1.color = "lightgrey";
                tab3.color = "lightgrey";
                tab4.color = "lightgrey";
                loadcom.source = "ProgramMap.qml";
            }
        }
    }
    Rectangle{
        id: tab3
        width: 120
        height: 45
        color: "lightgrey"
        Text{
            text: "Courses List"
            color: "white"
            font{
                pixelSize: 14
                bold: false
                family: "Bebas Neue"
            }
            anchors{
                top: parent.top
                topMargin: 5
                //bottomMargin: 5
                horizontalCenter: parent.horizontalCenter
            }
        }
        anchors{
            top: parent.top
            left: tab2.right
            leftMargin: 5
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                parent.color = "#8a001e";
                tab1.color = "lightgrey";
                tab2.color = "lightgrey";
                tab4.color = "lightgrey";
                loadcom.source = "CourseList.qml";
            }
        }
    }
    Rectangle{
        id: tab4
        width: 120
        height: 45
        color: "lightgrey"
        Text{
            text: "Search Course"
            color: "white"
            font{
                pixelSize: 14
                bold: false
                family: "Bebas Neue"
            }
            anchors{
                top: parent.top
                topMargin: 5
                //bottomMargin: 5
                horizontalCenter: parent.horizontalCenter
            }
        }
        anchors{
            top: parent.top
            left: tab3.right
            leftMargin: 5
        }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                parent.color = "#8a001e";
                tab1.color = "lightgrey";
                tab2.color = "lightgrey";
                tab3.color = "lightgrey";
                loadcom.source = "SearchCourse.qml";
            }
        }
    }
    Rectangle{
        anchors{
            top: parent.top
            topMargin: 30
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
//        border.color: "lightgrey"
        Loader{
            id: loadcom
            anchors.margins:5
            source: "StudentEnroll.qml"
            anchors.fill: parent
        }
    }
}
