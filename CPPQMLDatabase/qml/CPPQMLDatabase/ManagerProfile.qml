import QtQuick 2.0

Item {
    height: 40
    Text{
        id: sid
        width: 117
        height: 17
        anchors{
            left: parent.left
            leftMargin: 248
            top: parent.top
        }
        color: "#8a001e"
        text: "Manager ID: " + Session.username
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
    Text{
        id: name
        width: 261
        height: 17
        text: "Manager Name: " + Session.name
        anchors{
            left: parent.left
            leftMargin: 10
            top: parent.top
        }
        color: "#8a001e"
        font{
            pixelSize: 14
            bold: false
            family: "Champagne & Limousines"
        }
    }
}

