import QtQuick 2.0
ListView {
    anchors.top: parent.bottom
    orientation: ListView.Horizontal
    id: root
//    property bool isDeletable: false
    model: StudentRes
    delegate:Item {
        id: del
        width: parent.width
        height: parent.height

        Component{
            id: rectview
            Rectangle{
                width:  loadWidth
                height: 25
                Text{
                    anchors {
                        left: parent.left
                        leftMargin: 35
                        top: parent.top
                    }

                    id: nameText
                    color: "#8a001e"
                    opacity: 0.9
                    text: name
                    wrapMode: Text.WordWrap
                    font{
                        pixelSize: 19
                        family: "Red World"
                        bold: false
                    }
                }
            }
        }
        Loader{
            id: loadSID;
            property int loadWidth: del.width
            property string name: s_id === "" ? "Not found." : s_id;
            //property bool isShow: isDeletable;
            sourceComponent: rectview;
            anchors{left: parent.left}
        }
        Loader{
            id: loadSName;
            property int loadWidth: del.width
            property string name: "Student Name: "+ s_name;
            //property bool isShow: false;
            sourceComponent: rectview;
            anchors{
                left: loadSID.right
            }
        }
    }
}
